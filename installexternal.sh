#!/bin/bash

ENABLE_MPI="n"
ENABLE_DEBUG="n"

# if set to "n" only the trunk version of DUNE will compile with the
# UG grid manager
UG_SUPPORT_DUNE_20="y"
CLEANUP="n"

TOPDIR=$(pwd)

installUG()
{
    if [ ! -d external ]; then
        mkdir external
    fi
    cd external
    wget http://conan.iwr.uni-heidelberg.de/download/ug-3.11.0.tar.gz

    if  test "$CLEANUP" == "y"; then
        rm -rf ug
        return
    fi

    if ! test -e "ug-3.11.0"; then
        tar zxvf ug-3.11.0.tar.gz
    fi

    cd ug-3.11.0
    autoreconf -is
    OPTIM_FLAGS="-O3 -DNDEBUG -march=native -finline-functions -funroll-loops"
    # debug flags
    if test "$ENABLE_DEBUG" == "y"; then 
        OPTIM_FLAGS="-O0 -g2"
    fi
    CFLAGS="$OPTIM_FLAGS"
    CXXFLAGS="$OPTIM_FLAGS -std=c++0x -fno-strict-aliasing"
    OPTS="--enable-dune --prefix=$PWD"

    if test "$ENABLE_MPI" == "y"; then
        OPTS="$OPTS --enable-parallel MPICC=$MPICXX"
    else
        OPTS="$OPTS --without-mpi"
    fi

    ./configure \
        CC=g++ \
        CFLAGS="$CFLAGS" \
        CXXFLAGS="$CXXFLAGS" \
        $OPTS
    make
    make install

    cd $TOPDIR
}

usage()
{
    echo "Usage: $0 [OPTIONS] PACKAGES"
    echo ""
    echo "Where PACKAGES is one or more of the following"
    echo "  ug       Install the UG grid library."
    echo ""
    echo "The following options are recoginzed:"
    echo "    --parallel       Enable parallelization if available."
    echo "    --debug          Compile with debugging symbols and without optimization."
    echo "    --clean          Delete all files for the given packages."
}

SOMETHING_DONE="n"
for TMP in "$@"; do
    TMP=$(echo "$TMP" | tr "[:upper:]" "[:lower:]")
    case $TMP in 
        "--debug")
            ENABLE_DEBUG="y"
            ;;
        "--parallel")
            ENABLE_MPI="y"
            MPICC=$(which mpicc)
            MPICXX=$(which mpicxx)
            MPIF77=$(which mpif77)

            if test -f $(pwd)'/../dune-common/bin/mpi-config'; then 
                MPICONFIG=$(pwd)'/../dune-common/bin/mpi-config'
            else
                if test -f $(pwd)'/../dune-common-2.0/bin/mpi-config'
                then
                    MPICONFIG=$(pwd)'/../dune-common-2.0/bin/mpi-config'
                else 
                    echo "MPICONFIG not found!"
            return
                fi
            fi 

            MPILIBS=$($MPICONFIG --libs)
            MPILIBDIR=$(echo $MPILIBS | sed "s/.*-L\([^[:blank:]]*\).*/\1/")

            # consistency check 
            if test "$ENABLE_MPI" == "y" -a -z "$MPICXX"; then 
                echo ""
                echo "Compiler mpicxx not found although ENABLE_MPI is set in this script!"
                echo "Please make sure that your MPI environment is set up or that you turn it off."
                echo "The shell command mpi-selector may help you to select an installed mpi-version."
                echo "Reinitilize your PATH variable after using it (e.g. logout and login again)."
                echo "Due to this error this script stops further building now."
                echo ""
                exit -1
            fi

            ;;
        "--clean")
            CLEANUP="y"
            ;;
        ug)
            SOMETHING_DONE="y"
            installUG
            ;;

        *)
            usage
            exit 1
    esac
done

if test "$SOMETHING_DONE" != "y"; then
    usage
    exit 1;
fi

