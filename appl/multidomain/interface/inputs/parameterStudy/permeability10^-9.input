#############################################################
#Configuration file for test_2cnizeroeq2p2cni
#############################################################

#############################################################
#General conditions
#############################################################

[Problem]
#First part of the interface, where it is not coupled;
#set to zero or negative if not desired
RunUpDistanceX = 0.251
RunUpDistanceX2 = 0.499
NoDarcyX = 0.25
NoDarcyX2 = 0.5

[TimeManager]
#Initial and maximum time-step size
DtInitial = 5e-5
MaxTimeStepSize = 360           # 30

#Initialization time without coupling
#set to zero or negative if not desired
InitTime = 0                            # 864

#Simulation end
TEnd = 0.8e6  
#Define the length of an episode (for output)
EpisodeLength = 43200 # one hour:3600 #half day:43200
OnlyAtEpisodeLength = false # data for storage.out written out only every EpisodeLength

[Vtk]
#Names for VTK output
NameFF = zeroeq2cni
NamePM = 2p2cni

[Grid]
UseInterfaceMeshCreator = true

#Name of the dgf file (grid)
File = grids/interfacedomain.dgf
Refinement = 0

#Number of elements in x-, y-, z-direction
CellsX = 60              #30    #  50   # dx     = 1.00e-2 m
CellsY = 96              #72    # 120   # hy,min = 1.06E-4 m
CellsZ = 1
#Grading and refinement of the mesh
Grading = 1.16               # 1.1
RefineTop = true

#Extend of entire domain
XMin = 0.0
XMax = 0.75
YMin = 0.0
YMax = 0.75

#Vertical position of coupling interface
InterfacePos = 0.25

[Output]
#Frequency of restart file, flux and VTK output
FreqRestart = 500            # how often restart files are written out
FreqOutput = 5               #  10  # frequency of VTK output
FreqMassOutput = 5           #  20  # frequency of mass and evaporation rate output (Darcy)
FreqFluxOutput = 100         # 100  # frequency of detailed flux output
FreqVaporFluxOutput = 3      #   5  # frequency of summarized flux output

#############################################################
#Define conditions in the two subdomains
#############################################################
[Stokes]
StabilizationAlpha = -1.0

[FreeFlow]
RefPressure = 1e5
RefTemperature = 298.15
RefMassfrac = 0.008          # 0.01
VxMax = 3.5
VelocityProfile = 1          # 0 parabolic, 1 block, 2 power, 3 linear
BeaversJosephSlipVel = 0.001
ExponentMTC = 0.0            # 1
UseBoundaryLayerModel = 0    # not implemented for ZeroEq
ConstThickness = 0.0016      # for a constant BL thickness, use BL model 9
BoundaryLayerOffset = 0.0    # for BL model like Blasius, determines a virtual run-up distance for the flow
YPlus = 50                   # 50, pre-factor for calculating the BL thickness, use BL model 4
MassTransferModel = 0        # not implemented for ZeroEq
implementAlpha2 = false      # multiply factor of 1/Alpha2 to the Beavers and Joseph Alpha
EnableDiffusiveEnthalpyFluxes = true

SinusVelVar = 0.0 	# hourly velocity variation
SinusPVar = 0.0 	# diurnal pressure variation
SinusTVar = 0.0 	# diurnal temperature variation
SinusXVar = 0.0 	# diurnal variation of the vapor concentration (massfraction)

[PorousMedium]
RefPressurePM = 1e5
RefTemperaturePM = 298.15
InitialSw1 = 0.98
InitialSw2 = 0.98

############################################################
#Define the spatial parameters
############################################################
[SpatialParams]
SoilType = 2				# int
MaterialInterfaceX = 100
AlphaBJ1 = 1.0
AlphaBJ2 = 1.0
AlphaBJ3 = 1.0
RegularizationThreshold = 1e-2

# GStat stuff, only required for SoilType=0
GenerateNewPermeability = true
GStatControlFileName = gstatControl_2D.txt
GStatInputFileName = gstatInput.txt
PermeabilityInputFileName = permeab.dat

### SoilType = 0 ###
[SpatialParams.Heterogeneous]
Porosity = 0.4
Swr = 0.005
Snr = 0.01
VgAlpha = 8.74e-4
VgN = 10.0
LambdaSolid = 5.26

### SoilType = 1 ### (Zurich coarse, MUSIS sand)
[SpatialParams.Coarse]
Permeability1 = 7.0e-10       #Kozeny-Carman; 1.4e-11
Porosity1 = 0.44
Swr1 = 0.005
Snr1 = 0.01
VgAlpha1 = 8.74e-4
VgN1 = 3.35
PlotMaterialLaw1 = false
LambdaSolid1 = 5.26

Pentry1 = 1012
BCLambda1 = 3.277

### SoilType = 2 ###
# d_min = 0.3e-3
# d_max = 0.9e-3
[SpatialParams.Medium]
Permeability2 = 2.65e-9
Porosity2 = 0.41
Swr2 = 0.005
Snr2 = 0.01
VgAlpha2 = 6.371e-4
VgN2 = 8.0
PlotMaterialLaw2 = false
LambdaSolid2 = 5.26

### SoilType = 3 ### (Colorado)
[SpatialParams.Fine]
Permeability3 = 1.06e-10
Porosity3 = 0.334
Swr3 = 0.028
Snr3 = 0.01
VgAlpha3 = 5.81e-4
VgN3 = 17.8
PlotMaterialLaw3 = false
LambdaSolid3 = 5.26

#############################################################
# Newton parameters
#############################################################
[Newton]
RelTolerance = 1e-5
TargetSteps = 8
MaxSteps = 12
WriteConvergence = false
MaxTimeStepDivisions = 20 # should be in group Implicit

#############################################################
# Linear solver parameters
#############################################################
[LinearSolver]
ResidualReduction = 1e-9
Verbosity = 0
MaxIterations = 100

#[Pardiso]
#NumProcessors = 1

############################################################
# Eddy Viscosity Models
# 0 = none
# 1 = Prandtl
# 2 = Deissler near wall law
# 3 = Michel
# 4 = Baldwin Lomax
# 5 = Prandtl Van Driest
# 6 = no name (from Lehfeldt)
# 7 = Perrels and Karrelse
# 8 = indirect eddy viscosity formulation
# 9 = modified Prandtl Van Driest
############################################################
############################################################
# Eddy Diffusivity and Conductivity Models
# 0 = none
# 1 = Reynolds analogy (eddy viscosity = eddy diffusivity)
# 2 = modified PrandtlVanDriest
# 3 = Meier
# 4 = Rohensow and Chohen
# 5 = Exponential
# 6 = Henderson
# 7 = Deissler
############################################################
[ZeroEq]
SCVDataWriteAll = false
SCVDataCoordinateDelta = 0.005
FrequencyVtkFiles = 10000

EddyViscosityModel = 9 				# 4 or 9
BBoxMinStricklerValue = 100         # only for Perrels and Karelse
BBoxMaxStricklerValue = 100         # only for Perrels and Karelse
BBoxMinSandGrainRoughness = 0.0     # 0.6e-3
BBoxMaxSandGrainRoughness = 0.0     # 0

EddyDiffusivityModel = 7
MExponentialDiffusivity = 18.0      # 18.0 Perrels

EddyConductivityModel = 7
MExponentialConductivity = 18.0     # 18.0 Perrels
