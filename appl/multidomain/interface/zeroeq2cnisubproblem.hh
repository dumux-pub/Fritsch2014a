// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \ingroup BoxZeroEq2cniProblem
 * \brief  Definition of a non-isothermal compositional ZeroEq subproblem
 */
#ifndef DUMUX_ZEROEQ2CNI_SUBPROBLEM_HH
#define DUMUX_ZEROEQ2CNI_SUBPROBLEM_HH

#include <dune/pdelab/finiteelementmap/conformingconstraints.hh>

#include <dumux/freeflow/zeroeq2cni/zeroeq2cnimodel.hh>
#include <dumux/multidomain/couplinglocalresiduals/stokesncnicouplinglocalresidual.hh>
#include <dumux/multidomain/common/subdomainpropertydefaults.hh>

namespace Dumux
{

template <class TypeTag>
class ZeroEq2cniSubProblem;

//////////
// Specify the properties for the ZeroEq2cni problem
//////////
namespace Properties
{
NEW_TYPE_TAG(ZeroEq2cniSubProblem,
             INHERITS_FROM(BoxZeroEq2cni, SubDomain, TwoCNIZeroEqTwoPTwoCNISpatialParams));

//! Set the problem property
SET_TYPE_PROP(ZeroEq2cniSubProblem, Problem, Dumux::ZeroEq2cniSubProblem<TypeTag>);

//! Set the property for the material parameters by extracting it from the material law
SET_PROP(ZeroEq2cniSubProblem, MaterialLawParams)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
public:
    typedef typename MaterialLaw::Params type;
};

//! Use the StokesncniCouplingLocalResidual for the computation of the local residual in the Stokes domain
SET_TYPE_PROP(ZeroEq2cniSubProblem,
              LocalResidual,
              StokesncniCouplingLocalResidual<TypeTag>);

SET_TYPE_PROP(ZeroEq2cniSubProblem, Constraints, Dune::PDELab::NonoverlappingConformingDirichletConstraints);

// set the grid operator
SET_PROP(ZeroEq2cniSubProblem, GridOperator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, ConstraintsTrafo) ConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, GridFunctionSpace) GridFunctionSpace;
    typedef Dumux::PDELab::MultiDomainLocalOperator<TypeTag> LocalOperator;

    enum{numEq = GET_PROP_VALUE(TypeTag, NumEq)};
public:
    typedef Dune::PDELab::GridOperator<GridFunctionSpace,
                                       GridFunctionSpace, LocalOperator,
                                       Dune::PDELab::ISTLBCRSMatrixBackend<numEq, numEq>,
                                       Scalar, Scalar, Scalar,
                                       ConstraintsTrafo, ConstraintsTrafo,
                                       true> type;
};

//! Set the fluid system
SET_PROP(ZeroEq2cniSubProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    typedef FluidSystem type;
};

//! Set number of intervals for the evaluation of flow properties at the wall
SET_INT_PROP(ZeroEq2cniSubProblem, NumberOfIntervals, 50);

//! Use formulation based on mass fractions
SET_BOOL_PROP(ZeroEq2cniSubProblem, UseMoles, false);

//! Disable gravity in the ZeroEq domain
SET_BOOL_PROP(ZeroEq2cniSubProblem, ProblemEnableGravity, false);

//! Set Calculation to Navier-Stokes
SET_BOOL_PROP(ZeroEq2cniSubProblem, EnableNavierStokes, true);
}

/*!
 * \ingroup BoxZeroEq2cniProblem
 * \brief Non-isothermal compositional ZeroEq problem with air flowing
 *        from the left to the right.
 *
 * This sub problem uses the \ref ZeroEq2cniModel.
 *
 */
template <class TypeTag>
class ZeroEq2cniSubProblem : public ZeroEqProblem<TypeTag>
{
    typedef ZeroEq2cniSubProblem<TypeTag> ThisType;
    typedef ZeroEqProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    // soil parameters for beavers & joseph
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    enum {
        // Number of equations and grid dimension
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        dim = GridView::dimension
    };
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // equation indices
        massBalanceIdx = Indices::massBalanceIdx,
        momentumXIdx = Indices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx = Indices::momentumZIdx, //!< Index of the z-component of the momentum balance
        transportEqIdx = Indices::transportEqIdx, //!< Index of the transport equation (massfraction)
        energyEqIdx =    Indices::energyEqIdx     //!< Index of the energy equation (temperature)
    };
    enum { // primary variable indices
        pressureIdx = Indices::pressureIdx,
        velocityXIdx = Indices::velocityXIdx,
        velocityYIdx = Indices::velocityYIdx,
        velocityZIdx = Indices::velocityZIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum { phaseIdx = Indices::phaseIdx };
    enum { numComponents = Indices::numComponents };
    enum {
        transportCompIdx = Indices::transportCompIdx, //!< water component index
        phaseCompIdx = Indices::phaseCompIdx          //!< air component index
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    /*!
     * \brief docme
     */
public:
    ZeroEq2cniSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView),
          spatialParams_(gridView)
    {
        try
        {
            bBoxMin_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, XMin);
            bBoxMax_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, XMax);
            bBoxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
            bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, YMax);

            refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
            refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefPressure);
            refMassfrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassfrac);
            vxMax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, VxMax);
            bjSlipVel_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, BeaversJosephSlipVel);
            sinusVelVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelVar);
            velocityProfile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, unsigned, FreeFlow, VelocityProfile);

            sinusVelVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelVar);
            sinusPVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPVar);
            sinusTVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTVar);
            sinusXVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusXVar);

            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, MaterialInterfaceX);
            runUpDistanceX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, RunUpDistanceX); // first part of the interface without coupling
            runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, RunUpDistanceX2); // last part of the interface without coupling
            initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

            alphaBJ1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ1);
            alphaBJ2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ2);
            alphaBJ3_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ3);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    /* bBox functions have to be overwritten, otherwise they
     * remain uninitialised
     */
    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string &name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Vtk, NameFF); }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypes()
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition &globalPos = vertex.geometry().center();
        const Scalar time = this->timeManager().time();

        values.setAllDirichlet();

        if (onUpperBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setDirichlet(temperatureIdx);
        }


        if (onLowerBoundary_(globalPos))
        {
            values.setAllDirichlet();
            values.setNeumann(transportEqIdx);
            values.setDirichlet(temperatureIdx);

            if (globalPos[0] > runUpDistanceX_-eps_ && globalPos[0] < runUpDistanceX2_+eps_ && time < initializationTime_)
            {
                values.setDirichlet(massOrMoleFracIdx);
                values.setDirichlet(temperatureIdx);
            }

            if (globalPos[0] > runUpDistanceX_-eps_ && globalPos[0] < runUpDistanceX2_+eps_ && time >= initializationTime_)
            {
                values.setAllCouplingOutflow();
            }
        }

        if (onRightBoundary_(globalPos))
        {
            values.setDirichlet(massOrMoleFracIdx, transportEqIdx);
            values.setDirichlet(temperatureIdx, energyEqIdx);
          
//             values.setAllOutflow();
// 
//             if (onUpperBoundary_(globalPos)) // corner point
//                 values.setAllDirichlet();
        }
        
        // Left inflow boundaries should be Neumann, otherwise the
        // evaporative fluxes are much more grid dependent
        if (onLeftBoundary_(globalPos))
        {
//             values.setNeumann(transportEqIdx);
//             values.setDirichlet(temperatureIdx);
            
            values.setDirichlet(massOrMoleFracIdx, transportEqIdx);
            values.setDirichlet(temperatureIdx, energyEqIdx);

            if (onUpperBoundary_(globalPos) || onLowerBoundary_(globalPos)) // corner point
                values.setAllDirichlet();
        }

        // the mass balance has to be of type outflow
        // it does not get a coupling condition, since pn is a condition for stokes
        values.setOutflow(massBalanceIdx);

        // set pressure at one point, do NOT specify this
        // if the Darcy domain has a Dirichlet condition for pressure
        if (onRightBoundary_(globalPos))
        {
            if (time > initializationTime_)
                values.setDirichlet(pressureIdx, massBalanceIdx);
            else
                if (!onLowerBoundary_(globalPos) && !onUpperBoundary_(globalPos))
                    values.setDirichlet(pressureIdx, massBalanceIdx);
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichlet()
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        const Scalar time = this->timeManager().time();

        values = 0.0;
//        initial_(values, globalPos);

        if (time > initializationTime_)
            values[velocityXIdx] = xVelocity_(globalPos);
        values[velocityYIdx] = 0.0;
        values[pressureIdx] = refPressure()
            + 1.189 * this->gravity()[1] * (globalPos[1] - bBoxMin_[1]);
        values[massOrMoleFracIdx] = refMassfrac();
        values[temperatureIdx] = refTemperature();

//        if (onLeftBoundary_(globalPos))
//            values [transportEqIdx] = refMassfrac_ - 0.001 *
//            (globalPos[1] - bBoxMin_[1])*(bBoxMax_[1] - globalPos[1])
//                            / (0.25*height_()*height_());
    }

    //! \copydoc Dumux::ImplicitProblem::neumann()
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =
                fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;

        values = 0.;

        FluidState fluidState;
        fluidState.setTemperature(refTemperature_);
        fluidState.setPressure(phaseIdx, refPressure_);

        Scalar massFraction[numComponents];
        massFraction[transportCompIdx] = refMassfrac_;
        massFraction[phaseCompIdx] = 1 - massFraction[transportCompIdx];

        // calculate average molar mass of the gas phase
        Scalar M1 = FluidSystem::molarMass(transportCompIdx);
        Scalar M2 = FluidSystem::molarMass(phaseCompIdx);
        Scalar X2 = massFraction[phaseCompIdx];
        Scalar massToMoleDenominator = M2 + X2*(M1 - M2);

        fluidState.setMoleFraction(phaseIdx, transportCompIdx, massFraction[transportCompIdx]*M2/massToMoleDenominator);
        fluidState.setMoleFraction(phaseIdx, phaseCompIdx, massFraction[phaseCompIdx]*M1/massToMoleDenominator);

        const Scalar density =
                FluidSystem::density(fluidState, phaseIdx);
        const Scalar enthalpy =
                FluidSystem::enthalpy(fluidState, phaseIdx);
        const Scalar xVelocity = xVelocity_(globalPos);

        if (onLeftBoundary_(globalPos)
            && globalPos[1] > bBoxMin_[1] && globalPos[1] < bBoxMax_[1])
        {
            values[transportEqIdx] = -xVelocity*density*(refMassfrac_);// - 0.001 *
//                    (globalPos[1] - bBoxMin_[1])*(bBoxMax_[1] - globalPos[1])
//                                    / (0.25*height_()*height_()));
            values[energyEqIdx] = -xVelocity*density*enthalpy;
        }
    }

    /*!
     * \brief Evaluate the Beavers-Joseph coefficient
     *        at the center of a given intersection
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param is The intersection between element and boundary
     * \param scvIdx The local subcontrolvolume index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * \return Beavers-Joseph coefficient
     */
    Scalar beaversJosephCoeff(DimVector globalPos/*const Element &element,
                              const FVElementGeometry &fvGeometry,
//                               const Intersection &is,
                              const int scvIdx,
                              const int boundaryFaceIdx*/) const
    {
        if (onLowerBoundary_(globalPos))//< && globalPos[0] > runUpDistanceX_-eps_)
            return alphaBJ2_;
        else
            return 0.0;
    }

    /*!
     * \brief Evaluate the intrinsic permeability
     *        at the corner of a given element
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scvIdx The local subcontrolvolume index
     *
     * \return permeability in x-direction
     */
    Scalar permeability(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return spatialParams_.intrinsicPermeability(element,
                                                    fvGeometry,
                                                    scvIdx);
    }

    // \}

    //! \copydoc Dumux::ImplicitProblem::source()
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {
        // ATTENTION: The source term of the mass balance has to be chosen as
        // div (q_momentum) in the problem file
        values = Scalar(0);
    }

    //! \copydoc Dumux::ImplicitProblem::initial()
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos
            = element.geometry().corner(scvIdx);

        initial_(values, globalPos);
    }
    // \}

    /*!
     * \brief Determine if we are on a corner of the grid
     *
     * \param global Pos The global position
     *
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;
    }

    // required in case of mortar coupling
    // otherwise it should return false
    /*!
     * \brief docme
     *
     * \param global Pos The global position
     *
     */
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    { return false; }

    /*!
     * \brief Returns the spatial parameters object.
     */
    SpatialParams &spatialParams()
    { return spatialParams_; }
    const SpatialParams &spatialParams() const
    { return spatialParams_; }

    const Scalar refTemperature() const
    { return refTemperature_+ diurnalVariation_(sinusTVar_); }

    const Scalar refMassfrac() const
    { return refMassfrac_ + diurnalVariation_(sinusXVar_); }

    const Scalar refPressure() const
    { return refPressure_ + diurnalVariation_(sinusPVar_); }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)


    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[velocityXIdx] = xVelocity_(globalPos);
        values[velocityYIdx] = 0.;

        values[pressureIdx] = refPressure_
            + 1.189 * this->gravity()[1] * (globalPos[1] - bBoxMin_[1]);
        values[massOrMoleFracIdx] = refMassfrac_;
        values[temperatureIdx] = refTemperature_;
    }

    const Scalar xVelocity_(const GlobalPosition &globalPos) const
    {
        const Scalar vmax = vxMax_ + diurnalVariation_(sinusVelVar_);

        if (onUpperBoundary_(globalPos))
            return 0.0;
        if (onLowerBoundary_(globalPos))
            return bjSlipVel_;

        // parabolic profile
        if (velocityProfile_ == 0)
        {
            return  vmax * (globalPos[1] - bBoxMin_[1]) * (bBoxMax_[1] - globalPos[1])
                    / (0.25 * height_() * height_());
        }

        // block profile
        if (velocityProfile_ == 1)
        {
            return vmax;
        }

        // power profile
        if (velocityProfile_ == 2)
        {
            Scalar exponent = 1.0 / 5.0;
            Scalar middle = (bBoxMax_[1] + bBoxMin_[1]) / 2.0;
            Scalar radius = (middle - bBoxMin_[1]);
            return vmax * pow(1 - fabs(globalPos[1] - middle) / radius, exponent);
        }

        // linear profile
        if (velocityProfile_ == 3)
        {
            return vmax * (bBoxMax()[1] - globalPos[1])
                   / (bBoxMax()[1] - bBoxMin()[1]);
        }

        //        const Scalar relativeHeight = (globalPos[1]-bBoxMin_[1])/height_();
        // linear profile
        //        return vmax*relativeHeight + bjSlipVel_; // BJ slip velocity is added as sqrt(Kxx)
        // parabolic profile
        return  vmax*(globalPos[1] - bBoxMin_[1])*(bBoxMax_[1] - globalPos[1])
            / (0.25*height_()*height_()) + bjSlipVel_;
        // logarithmic profile
        //        return 0.1*vmax*log((relativeHeight+1e-3)/1e-3) + bjSlipVel_;
    }

    const Scalar diurnalVariation_(const Scalar value) const
    {
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        return sin(2*M_PI*time/86400) * value;
    }

    const Scalar hourlyVariation_(const Scalar value) const
    {
        const Scalar time = this->timeManager().time();
        return sin(2*M_PI*time/3600) * value;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    const Scalar height_() const
    { return bBoxMax_[1] - bBoxMin_[1]; }

    // spatial parameters
    SpatialParams spatialParams_;

    static constexpr Scalar eps_ = 1e-8;

    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;

    Scalar refPressure_;
    Scalar refTemperature_;
    Scalar refMassfrac_;

    Scalar vxMax_;
    Scalar bjSlipVel_;
    unsigned velocityProfile_;
    Scalar alphaBJ1_;
    Scalar alphaBJ2_;
    Scalar alphaBJ3_;

    Scalar sinusVelVar_;
    Scalar sinusPVar_;
    Scalar sinusTVar_;
    Scalar sinusXVar_;

    Scalar xMaterialInterface_;
    Scalar runUpDistanceX_;
    Scalar runUpDistanceX2_;
    Scalar initializationTime_;
};
} //end namespace

#endif // DUMUX_ZEROEQ2CNI_SUBPROBLEM_HH
