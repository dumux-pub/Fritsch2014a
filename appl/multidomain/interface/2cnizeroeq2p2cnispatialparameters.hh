/*****************************************************************************
 *   Copyright (C) 2011 by Klaus Mosthaf                                     *
 *   Copyright (C) 2010 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TWOCNIZEROEQ2P2CNISPATIALPARAMS_HH
#define DUMUX_TWOCNIZEROEQ2P2CNISPATIALPARAMS_HH

#include <dune/grid/io/file/vtk/common.hh>
#include "dumux/material/spatialparameters/gstatrandompermeability.hh"

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearizedregvangenuchtenparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/io/plotfluidmatrixlaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class TwoCNIZeroEqTwoPTwoCNISpatialParams;

namespace Properties
{
//! The spatial parameters TypeTag
NEW_TYPE_TAG(TwoCNIZeroEqTwoPTwoCNISpatialParams);

//! Set the spatial parameters
SET_TYPE_PROP(TwoCNIZeroEqTwoPTwoCNISpatialParams, SpatialParams,
              TwoCNIZeroEqTwoPTwoCNISpatialParams<TypeTag>);

//! Set the material Law
SET_PROP(TwoCNIZeroEqTwoPTwoCNISpatialParams, MaterialLaw)
{
 private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    //    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
    typedef RegularizedVanGenuchten<Scalar, LinearizedRegVanGenuchtenParams<Scalar, TypeTag> > EffMaterialLaw;
 public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};
}


/** \todo Please doc me! */
template<class TypeTag>
class TwoCNIZeroEqTwoPTwoCNISpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;

    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

//    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
//    typedef RegularizedVanGenuchten<Scalar> EffMaterialLaw;
    typedef RegularizedVanGenuchten<Scalar, LinearizedRegVanGenuchtenParams<Scalar, TypeTag> > EffMaterialLaw;
    typedef std::vector<Scalar> PermeabilityType;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

public:
    typedef EffToAbsLaw<EffMaterialLaw> MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
    typedef std::vector<MaterialLawParams> MaterialLawParamsVector;


    TwoCNIZeroEqTwoPTwoCNISpatialParams(const GridView& gridView)
        : ParentType(gridView),
          heterogeneousPermeability_(gridView.size(dim), 0.0),
          heterogeneousPorosity_(gridView.size(dim), 0.0),
          indexSet_(gridView.indexSet())
    {
        try
        {
            soilType_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, SoilType);
            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, MaterialInterfaceX);

            // porosities
//             heterogeneousPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Heterogeneous, Porosity);
            coarsePorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Porosity1);
            mediumPorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Porosity2);
            finePorosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Porosity3);

            // intrinsic permeabilities
            coarsePermeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Permeability1);
            mediumPermeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Permeability2);
            finePermeability_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Permeability3);

            // thermal conductivity of the solid material
            heterogeneousLambdaSolid_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Heterogeneous, LambdaSolid);
            coarseLambdaSolid_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, LambdaSolid1);
            mediumLambdaSolid_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, LambdaSolid2);
            fineLambdaSolid_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, LambdaSolid3);

            if (soilType_ != 0)
            {
                // residual saturations
                coarseParams_.setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Swr1));
                coarseParams_.setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Snr1));
                mediumParams_.setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Swr2));
                mediumParams_.setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Snr2));
                fineParams_.setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Swr3));
                fineParams_.setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Snr3));

                // parameters for the vanGenuchten law
                coarseParams_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, VgAlpha1));
                coarseParams_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, VgN1));
                mediumParams_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, VgAlpha2));
                mediumParams_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, VgN2));
                fineParams_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, VgAlpha3));
                fineParams_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, VgN3));

                // parameters for the Brooks-Corey law
                //            coarseParams_.setPe(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Pentry1));
                //            coarseParams_.setLambda(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, BCLambda1));
                //            mediumParams_.setPe(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Pentry2));
                //            mediumParams_.setLambda(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, BCLambda2));
                //            fineParams_.setPe(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Pentry3));
                //            fineParams_.setLambda(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, BCLambda3));
            }
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    ~TwoCNIZeroEqTwoPTwoCNISpatialParams()
    {}


    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element       The current finite element
     * \param fvGeometry    The current finite volume geometry of the element
     * \param scvIdx       The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        // heterogeneous parameter field computed with GSTAT
        if (soilType_ == 0)
            return heterogeneousPermeability_[indexSet_.index(
                    *(element.template subEntity<dim> (scvIdx)))];
        // soil can be chosen by setting the parameter soiltype accordingly
        else
        {
            const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

            if (checkSoilType(globalPos) == 1)
                return coarsePermeability_;
            if (checkSoilType(globalPos) == 3)
                return finePermeability_;
            else
                return mediumPermeability_;
        }
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        // heterogeneous parameter field computed with GSTAT
        if (soilType_ == 0)
            return heterogeneousPorosity_[indexSet_.index(
                    *(element.template subEntity<dim> (scvIdx)))];
        else
        {
          const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
          if (checkSoilType(globalPos) == 1)
              return coarsePorosity_;
          if (checkSoilType(globalPos) == 3)
              return finePorosity_;
          else
              return mediumPorosity_;
        }
    }


    /*!
     * \brief return the brooks-corey context depending on the position
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param faceIdx The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const int scvIdx) const
    {
        if (soilType_ == 0)
            return materialLawParams_[indexSet_.index(*(element.template subEntity<dim> (scvIdx)))];
        else
        {
            const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

            if (checkSoilType(globalPos)==1)
                return coarseParams_;
            if (checkSoilType(globalPos)==3)
                return fineParams_;
            else
                return mediumParams_;
        }
    }


    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    Scalar heatCapacity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return
            790 // specific heat capacity of granite [J / (kg K)]
            * 2650 // density of granite [kg/m^3]
            * (1 - porosity(element, fvGeometry, scvIdx));
    }

    Scalar thermalConductivitySolid(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        const GlobalPosition &pos = element.geometry().corner(scvIdx);

        if (checkSoilType(pos) == 0)
            return heterogeneousLambdaSolid_;
        if (checkSoilType(pos) == 1)
            return coarseLambdaSolid_;
        if (checkSoilType(pos) == 3)
            return fineLambdaSolid_;
        else
            return mediumLambdaSolid_;
    }

    const unsigned checkSoilType(const GlobalPosition &globalPos) const
    {
        // one soil, which can be chosen as runtime parameter:
        // 0: heterogeneous
        // 1: coarse
        // 2: medium
        // 3: fine
        return soilType_;
    }

    void loadIntrinsicPermeability(const GridView& gridView)
    {

        // only load random field, if soilType is set to 0
        if (soilType_ != 0)
            return;

        const unsigned size = gridView.size(dim);
        heterogeneousPermeability_.resize(size, 0.0);
        materialLawParams_.resize(size);

        bool create = true;
        bool setOnlyTopLayer = false;
        std::string gStatControlFileName("gstatControl_2D.txt");
        std::string gStatInputFileName("gstatInput.txt");
        std::string permeabilityFileName("permeab.dat");
        Scalar interfacePos = 0.0;
        try
        {
            create = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool , Gstat, GenerateNewPermeability);
            setOnlyTopLayer = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Gstat, SetOnlyTopLayer);
            gStatControlFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, ControlFileName);
            gStatInputFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, InputFileName);
            permeabilityFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, PermeabilityInputFileName);
            interfacePos = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        // create random permeability object
        GstatRandomPermeability<GridView, Scalar> randomPermeability(gridView,
                                                                     create,
                                                                     permeabilityFileName.c_str(),
                                                                     gStatControlFileName.c_str(),
                                                                     gStatInputFileName.c_str());

        Scalar totalVolume = 0;
        Scalar meanPermeability = 0;

        //TODO: only for 2D elements and rectangular grids
        const unsigned numVertices = 4;
        for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
        {
            ElementIterator eItEnd = gridView.template end<0> ();
            for (ElementIterator eIt = gridView.template begin<0> (); eIt
                     != eItEnd; ++eIt)
            {
                // for isotropic media
                const Scalar perm = randomPermeability.K(*eIt)[0][0];
                heterogeneousPermeability_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                    = perm;

                const Scalar volume = eIt->geometry().volume();
                meanPermeability += volume / perm;
                totalVolume += volume;
            }
        }
        meanPermeability /= totalVolume;
        meanPermeability = 1.0 / meanPermeability;

        ElementIterator eItEnd = gridView.template end<0> ();
        for (ElementIterator eIt = gridView.template begin<0> (); eIt
                  != eItEnd; ++eIt)
        {
            for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
            {
                GlobalPosition global = eIt->geometry().corner(vertexIdx);
                if (global[1] < interfacePos - 1e-6 && setOnlyTopLayer)
                {
                    heterogeneousPermeability_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                        = meanPermeability;
                }
                else
                {
                    heterogeneousPermeability_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                        = randomPermeability.K(*eIt)[0][0];
                }
            }
        }

        //Iterate over elements
        VertexIterator vItEnd = gridView.template end<dim> ();
        for (VertexIterator vIt = gridView.template begin<dim> (); vIt
                 != vItEnd; ++vIt)
        {
            int globalIdx = indexSet_.index(*vIt);

            // heterogeneous media with Van Genuchten
            // assumes Van-Genuchten alpha to be the inverse entry pressure
            Scalar elementEntryPressure = 1./GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Heterogeneous, VgAlpha);
            elementEntryPressure *= sqrt(meanPermeability / heterogeneousPermeability_[globalIdx]);

            materialLawParams_[globalIdx].setVgAlpha(1./elementEntryPressure);
            materialLawParams_[globalIdx].setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Heterogeneous, VgN));
            materialLawParams_[globalIdx].setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Heterogeneous, Swr));
            materialLawParams_[globalIdx].setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Heterogeneous, Snr));
        }

        //        if (create)
        //        {
        Dune::VTKWriter<GridView> vtkwriter(gridView);
        vtkwriter.addVertexData(heterogeneousPermeability_, "absolute permeability");
        PermeabilityType logPerm(size);
        for (unsigned int i = 0; i < size; i++)
            logPerm[i] = log10(heterogeneousPermeability_[i]);
        vtkwriter.addVertexData(logPerm, "logarithm of permeability");
        vtkwriter.write("permeability", Dune::VTK::OutputType::ascii);
    }
    
        void loadPorosity(const GridView& gridView)
    {
        // only load random field, if soilType is set to 0
        if (soilType_ != 0)
            return;

        const unsigned size = gridView.size(dim);
        heterogeneousPorosity_.resize(size, 0.0);
        materialLawParams_.resize(size);

        bool create = true;
        bool setOnlyTopLayer = false;
        std::string gStatControlFileName("gstatControl_2D.txt");
        std::string gStatInputFileName("gstatInput.txt");
        std::string permeabilityFileName("poros.dat");
        Scalar interfacePos = 0.0;
        try
        {
            create = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool , Gstat, GenerateNewPermeability);
            setOnlyTopLayer = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Gstat, SetOnlyTopLayer);
            gStatControlFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, ControlFileName);
            gStatInputFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, InputFileName);
            permeabilityFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, PorosityInputFileName);
            interfacePos = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        // create random permeability object
        GstatRandomPermeability<GridView, Scalar> randomPermeability(gridView,
                                                                     create,
                                                                     permeabilityFileName.c_str(),
                                                                     gStatControlFileName.c_str(),
                                                                     gStatInputFileName.c_str());

        Scalar totalVolume = 0;
        Scalar meanPermeability = 0;

        //TODO: only for 2D elements and rectangular grids
        const unsigned numVertices = 4;
        for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
        {
            ElementIterator eItEnd = gridView.template end<0> ();
            for (ElementIterator eIt = gridView.template begin<0> (); eIt
                     != eItEnd; ++eIt)
            {
                // for isotropic media
                const Scalar perm = log10(randomPermeability.K(*eIt)[0][0]);
                heterogeneousPorosity_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                    = perm;

                const Scalar volume = eIt->geometry().volume();
                meanPermeability += volume / perm;
                totalVolume += volume;
            }
        }
        meanPermeability /= totalVolume;
        meanPermeability = 1.0 / meanPermeability;

        ElementIterator eItEnd = gridView.template end<0> ();
        for (ElementIterator eIt = gridView.template begin<0> (); eIt
                  != eItEnd; ++eIt)
        {
            for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
            {
                GlobalPosition global = eIt->geometry().corner(vertexIdx);
                if (global[1] < interfacePos - 1e-6 && setOnlyTopLayer)
                {
                    heterogeneousPorosity_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                        = meanPermeability;
                }
                else
                {
                    heterogeneousPorosity_[indexSet_.index(*(eIt->template subEntity<dim> (vertexIdx)))]
                        = log10(randomPermeability.K(*eIt)[0][0]);
                }
            }
        }

        //        if (create)
        //        {
        Dune::VTKWriter<GridView> vtkwriter(gridView);
        vtkwriter.addVertexData(heterogeneousPorosity_, "porosity");
        vtkwriter.write("porosity", Dune::VTK::OutputType::ascii);
    }

    // this is called from the coupled problem
    // and creates a gnuplot output of the Pc-Sw curve
    void plotMaterialLaw()
    {
        if (soilType_ == 0)
        {
            std::cout << "Material law plot not possible for heterogeneous media!\n";
            return;
        }
        if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams.Coarse, PlotMaterialLaw1))
        {
            PlotFluidMatrixLaw<MaterialLaw> coarseFluidMatrixLaw_;
            coarseFluidMatrixLaw_.plotpC(coarseParams_,
                                         GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Swr1),
                                         1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Coarse, Snr1));
        }
        if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams.Medium, PlotMaterialLaw2))
        {
            PlotFluidMatrixLaw<MaterialLaw> mediumFluidMatrixLaw_;
            mediumFluidMatrixLaw_.plotpC(mediumParams_,
                                         GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Swr2),
                                         1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Medium, Snr2));
        }
        if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams.Fine, PlotMaterialLaw3))
        {
            PlotFluidMatrixLaw<MaterialLaw> fineFluidMatrixLaw_;
            fineFluidMatrixLaw_.plotpC(fineParams_,
                                       GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Swr3),
                                       1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams.Fine, Snr3));
        }
    }

private:

    unsigned soilType_;

    Scalar coarsePermeability_;
    Scalar mediumPermeability_;
    Scalar finePermeability_;

//     Scalar heterogeneousPorosity_;
    Scalar coarsePorosity_;
    Scalar mediumPorosity_;
    Scalar finePorosity_;

    Scalar heterogeneousLambdaSolid_;
    Scalar coarseLambdaSolid_;
    Scalar mediumLambdaSolid_;
    Scalar fineLambdaSolid_;

    Scalar xMaterialInterface_;
    MaterialLawParamsVector materialLawParams_;
    PermeabilityType heterogeneousPermeability_;
    PermeabilityType heterogeneousPorosity_;
    const IndexSet& indexSet_;

    MaterialLawParams coarseParams_;
    MaterialLawParams mediumParams_;
    MaterialLawParams fineParams_;
};

} // end namespace
#endif // DUMUX_TWOCNIZEROEQ2P2CNISPATIALPARAMS_HH
