// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012-2013 by Thomas Fetzer                                *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief This file contains the data which is required to calculate the
 *        fluxes over a face of a finite volume.
 *        This means concentration gradients, diffusion coefficients, mass fractions, etc.
 *        at the integration point.
 */
#ifndef DUMUX_ZEROEQ_FLUX_VARIABLES_HH
#define DUMUX_ZEROEQ_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/freeflow/stokes/stokesfluxvariables.hh>
#include <dumux/freeflow/zeroeq/zeroeqproperties.hh>

namespace Dumux
{

/*!
 * \ingroup StokesModel
 * \ingroup ZeroEqModel
 * \brief This template class contains data which is required to
 *        calculate the component fluxes over a face of a finite
 *        volume for a ZeroEq model.
 *
 * This means concentration gradients, diffusion coefficient, mass fractions, etc.
 * at the integration point of a scv or boundary face.
 */
template <class TypeTag>
class ZeroEqFluxVariables : public GET_PROP_TYPE(TypeTag, BaseStokesFluxVariables)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseStokesFluxVariables) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

public:
    ZeroEqFluxVariables(const Problem &problem,
                          const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const int faceIdx,
                          const ElementVolumeVariables &elemVolVars,
                          const bool onBoundary = false)
        : ParentType(problem, element, fvGeometry, faceIdx, elemVolVars, onBoundary)
        , flowNormal_(GET_PARAM(TypeTag, int, FlowNormal))
        , wallNormal_(GET_PARAM(TypeTag, int, WallNormal))
        , eddyViscosityModel_(GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel))
        , karmanConstant_(0.41)
    {
        globalPos_ = this->face().ipGlobal;
        dynamicEddyViscosity_ = Scalar(0);
        mixingLength_ = Scalar(0);
        viscosityInner_ = Scalar(0);
        viscosityOuter_ = Scalar(0);
        fz_ = Scalar(0);
        posIdx_ = problem.model().getPosIdx(globalPos_);
        wallIdx = problem.model().getWallIdx(globalPos_, posIdx_);
        
        runUpDistanceX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, RunUpDistanceX); // first part of the interface without coupling
        runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, RunUpDistanceX2); // last part of the interface without coupling
        
//      distanceToWallRough_ = std::abs(problem.model().distanceToWallRough(globalPos_, wallIdx, posIdx_));
        
        if (globalPos_[0] > runUpDistanceX_ - eps_ && globalPos_[0] < runUpDistanceX2_ + eps_)
            {
              distanceToWallRough_ = std::abs(problem.model().distanceToWallRough(globalPos_, wallIdx, posIdx_));
            }
            else
              distanceToWallRough_ = std::abs(problem.model().distanceToWallReal(globalPos_, wallIdx, posIdx_));
        
        distanceToWallReal_ = std::abs(problem.model().distanceToWallReal(globalPos_, wallIdx, posIdx_));
        for (int dimIdx = 0; dimIdx < dim; ++dimIdx)
            maxVelocity_[dimIdx] = problem.model().wall[wallIdx].maxVelocity[posIdx_][dimIdx];
        for (int dimIdx = 0; dimIdx < dim; ++dimIdx)
            minVelocity_[dimIdx] = problem.model().wall[wallIdx].minVelocity[posIdx_][dimIdx];
        velGrad_ = this->velocityGrad_[flowNormal_][wallNormal_];
        shearStressWall_ = problem.model().wall[wallIdx].wallShearStress[posIdx_];
        densityWall_ = problem.model().wall[wallIdx].wallDensity[posIdx_];
        frictionVelocityWall_ = sqrt(shearStressWall_ / densityWall_);
        yPlusReal_ = distanceToWallReal_ * frictionVelocityWall_ / problem.model().wall[wallIdx].wallKinematicViscosity[posIdx_];
        yPlusRough_ = distanceToWallRough_ * frictionVelocityWall_ / problem.model().wall[wallIdx].wallKinematicViscosity[posIdx_];

        if (GET_PARAM(TypeTag, bool, EnableNavierStokes))   // calculation of an eddy viscosity only makes sense WITH navier stokes term
            calculateEddyViscosity_(problem, element, elemVolVars);
    }

    /*!
    * \brief This functions calculates the eddy viscosity.
    *
    * The eddy viscosity is added to the viscosity in stokeslocalresidual.hh at each scv
    * face. 9 different eddy viscosity models are available.<br>
    * The dynamic viscosity is calculated.
    */
    void calculateEddyViscosity_(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars)
    {
        //////////////////////////
        // calculation of the DYNAMIC viscosity

        // 0 = no turbulence model
        // \mu_t = 0
        if (eddyViscosityModel_ == 0)
        {
            mixingLength_ = 0.;
            dynamicEddyViscosity_ = 0.;
        }

        // 1 = prandtl mixing length
        // \mu_t = \rho l^2 |dv_x/dy|
        // l = \kappa * y
        else if (eddyViscosityModel_ == 1)
        {
            mixingLength_ = distanceToWallRough_ * karmanConstant_;
            dynamicEddyViscosity_ = this->density() * mixingLength() * mixingLength() * std::abs(velGrad_);
        }

        // 2 = deissler near wall law
        // \mu_t = \beta ( 1 - std::exp( - \beta / \mu ) )
        // \beta = \rho * n^2 * v_x * y
        // n = 0.124 (deissler law constant)
        else if (eddyViscosityModel_ == 2)
        {
            const Scalar deisslerConstant = 0.124;
            const Scalar beta = this->density() * deisslerConstant * deisslerConstant * this->velocity()[flowNormal_] * distanceToWallRough_;
            dynamicEddyViscosity_ = beta * (1.0 - std::exp( - 1.0 * beta / this->dynamicViscosity()));
        }

        // 3 = michel
        // \mu_t = \rho l^2 |dv_x/dy|
        // l = \beta * tanh(\kappa * y / \beta)
        // \beta = \lambda * \delta
        else if (eddyViscosityModel_ == 3)
        {
            const Scalar lambda = 0.085;
            Scalar delta = 0.0;
            Scalar x = 0.0;

            if (this->velocity()[flowNormal_] > 0)
                x = std::abs(globalPos_[flowNormal_] - problem.bBoxMin()[flowNormal_]);
            else
                x = std::abs(globalPos_[flowNormal_] - problem.bBoxMax()[flowNormal_]);

            delta = std::abs(problem.model().wall[wallIdx].boundaryLayerThicknessCalculated[posIdx_]);
            // set a maximum value for \delta
            if (delta > std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]))
            {
                delta = std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]);
            }

            // eddy viscosity can only be calculated correctly for non-zero \delta
            Scalar beta = lambda * delta;
            if (beta > 0)
                mixingLength_ = beta * tanh(karmanConstant_ * distanceToWallRough_ / beta );
            else
                mixingLength_ = 0.0;

            dynamicEddyViscosity_ = this->density() * mixingLength() * mixingLength() * std::abs(velGrad_);
        }

        // 4 = baldwin/ lomax
        // \mu_inner = \rho * l^2 |w|
        // \mu_outer = \rho * K * c_cp * f_wake * f_kleb l
        //
        // the switching point between these equations is given by \mu_inner = \mu_outer
        // so we have to calculate the dynamic viscosity \mu
        else if (eddyViscosityModel_ == 4)
        {
            // LAW CONSTANTS
            const Scalar aPlus = 26;
            const Scalar cCP = 1.6;
            const Scalar cKleb = 0.30;
            const Scalar cWK = 0.25;
            const Scalar kUpper = 0.0168;

            // Calculate muInner
            mixingLength_ = karmanConstant_ * distanceToWallRough_ * (1 - std::exp( -1 * yPlusRough_ / aPlus ));
            Scalar omega1 = this->velocityGrad_[0][1] - this->velocityGrad_[1][0];
            Scalar omega2 = 0;
            Scalar omega3 = 0;
            if (dim == 3)
            {
                omega2 = this->velocityGrad_[1][2] - this->velocityGrad_[2][1];
                omega3 = this->velocityGrad_[2][0] - this->velocityGrad_[0][2];
            }
            Scalar omega = sqrt(omega1 * omega1 + omega2 * omega2 + omega3 * omega3);
            viscosityInner_ = this->density() * mixingLength() * mixingLength() * omega;

            // Calculate muOuter
            fz_ = distanceToWallRough_ * omega * (1 - std::exp( -1 * yPlusRough_ / aPlus ));
            Scalar fMax = problem.model().wall[wallIdx].fMax[posIdx_];
            Scalar yMax = std::abs(problem.model().wall[wallIdx].yMax[posIdx_]);
            Scalar temp[2] = {0.0, 0.0};
            for (int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                temp[0] += maxVelocity_[dimIdx] * maxVelocity_[dimIdx];
                temp[1] += minVelocity_[dimIdx] * minVelocity_[dimIdx];
            }
            Scalar uDiff = std::sqrt(temp[0]) - std::sqrt(temp[1]);

            Scalar f1 = yMax * fMax;
            Scalar f2 = cWK * yMax * uDiff * uDiff / fMax;
            Scalar fWake = fmin(f1, f2);
            Scalar fKleb = 1 / (1 + 5.5 * std::pow(cKleb * distanceToWallRough_ / yMax, 6.0));
            viscosityOuter_ = this->density() * kUpper * cCP * fWake * fKleb;

            bool inner = problem.model().useViscosityInner(globalPos_, posIdx_);

            // muOuter can only be calculated correctly for non-zero fmax
            if (fMax == 0)
                inner = true;

            if (inner)
                dynamicEddyViscosity_ = viscosityInner_;
            else
                dynamicEddyViscosity_ = viscosityOuter_;
        }

        // 5 = van-driest
        // \mu_t = \rho l^2 |dv_x/dy|
        // l = \kappa * y (1 - exp (- y^+ / A^+))
        // y^+ = \sqrt{ \rho_w \tau_w } * y / \mu
        else if (eddyViscosityModel_ == 5)
        {
            const Scalar aPlus = 26;
            mixingLength_ = karmanConstant_ * distanceToWallRough_ * (1 - std::exp( -1 * yPlusRough_ / aPlus ));
            dynamicEddyViscosity_  = this->density() * mixingLength() * mixingLength() * std::abs(velGrad_);
        }

        // 6 = channel formulation
        // \mu_t = \rho l^2 |dv_x/dy|
        // l = \kappa * y * \sqrt(1 - y / y_max)
        // lehfeldt p 50
        else if (eddyViscosityModel_ == 6)
        {
//            // combination of prandtls mixing length and l_ch resulting of channel flow consideration
//            Scalar distToWallMax = std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]);
//            if (1 - distanceToWallRough_ / distToWallMax < 0)
//                mixingLength_ = 0.0;
//            else
//                mixingLength_ = karmanConstant_ * distanceToWallRough_ * sqrt(1 - distanceToWallRough_ / distToWallMax);
//            dynamicEddyViscosity_  = this->density_ * mixingLength() * mixingLength() * std::abs(velGrad_);

            // straight derivation of \nu_t, after Lehfeldt
            Scalar distToWallMax = std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]);
            dynamicEddyViscosity_ = sqrt(shearStressWall_ / densityWall_) * karmanConstant_ * distanceToWallRough_ * (1 - distanceToWallRough_ /distToWallMax);
        }

        // 7 = perrels
        // lehfeldt p 52
        // C = k_st * r_hy ^ (1/6)
        // y_0 = [ 10 ^ (C/18) * 33 / (12 * R) ] ^ -1
        else if (eddyViscosityModel_ == 7)
        {
            // APPROACH 1 with Chezy formula from Helmig - Grundlagen
            // distanceToWall:   this model is with distanceToWallReal, as it includes already a surface roughness description
            Scalar stricklerValue = 100;
            if (problem.model().wall[wallIdx].isBBoxMinWall)
                try {
                    stricklerValue = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMinStricklerValue); // 100=smooth cement, 30= rough
                }
                catch (Dumux::ParameterException &e) {
                    std::cerr << e << ". Abort!\n";
                    exit(1) ;
                }
                catch (...) {
                     std::cerr << "Unknown exception thrown!\n";
                     exit(1);
                }
            else
                try {
                    stricklerValue = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMaxStricklerValue); // 100=smooth cement, 30= rough
                }
                catch (Dumux::ParameterException &e) {
                    std::cerr << e << ". Abort!\n";
                    exit(1) ;
                }
                catch (...) {
                     std::cerr << "Unknown exception thrown!\n";
                     exit(1);
                }
            Scalar distToWallMax = std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]);
            Scalar rHydraulic = distToWallMax; // as we consider almost infinite wide sections (more precise r=(a*b)/(a+b) with lim b -> 0)
            Scalar chezyValue = stricklerValue * std::pow(rHydraulic, .166666);
            Scalar yZero = 12 * rHydraulic / (std::pow(10.0, chezyValue/18.0) * 33);


//            // APPROACH 2 with Darcy friction factor and Haaland eqution (wiki - friction factor)
//            // distanceToWall:   this model is with distanceToWallReal, as it includes already a surface roughness description
//            const Scalar stricklerValue = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, StricklerValue); // 100=smooth cement, 30= rough
//            Scalar distToWallMax = std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]);
//            Scalar rHydraulic = distToWallMax; // as we consider almost infinite wide sections (more precise r=(a*b)/(a+b) with lim b -> 0)
//            Scalar reynolds = problem.model().wall[wallIdx].maxVelocity[posIdx_][flowNormal_] * rHydraulic * 2.0 / (this->dynamicViscosity() / this->density());
//            Scalar tmp = pow(problem.model().wall[wallIdx].sandGrainRoughness[posIdx_] / (rHydraulic * 2.0 * 3.7), 1.11);
//            tmp += 6.9 / reynolds;
//            tmp = log10(tmp);
//            tmp = 1 / (-1.8 * tmp);
//            Scalar lambda = tmp * tmp;
//            Scalar chezyValue = sqrt( 8.0 * 9.81  / lambda );
//            Scalar yZero = 12 * rHydraulic / (std::pow(10.0, chezyValue/18.0) * 33);

            if (distanceToWallReal_ < 0.25 * distToWallMax)
                mixingLength_ = karmanConstant_ * (distanceToWallReal_ + yZero);
            else
                mixingLength_ = karmanConstant_ * (0.25 * distToWallMax + yZero);
            dynamicEddyViscosity_  = this->density() * mixingLength() * mixingLength() * std::abs(velGrad_);
        }

        // 8 = indirect
        // schlichting formula chapter 17.133
        if (eddyViscosityModel_ == 8)
        {
            /* distanceToWall:   is only an empirical approach, valid for smooth walls */
            Scalar distToWallMax = std::abs(problem.model().wall[wallIdx].boundaryLayerThickness[posIdx_]);
            Scalar dimlessRadius = (1 - distanceToWallReal_ / distToWallMax);
            dynamicEddyViscosity_  =
                karmanConstant_ / 6.
                * (1 - dimlessRadius * dimlessRadius)
                * (1 + 2 * dimlessRadius * dimlessRadius)
                * frictionVelocityWall_
                * distToWallMax * this->density();
        }


        // 9 = modified prandtl van-driest
        if (eddyViscosityModel_ == 9)
        {
            Scalar aPlus = 26.0;
            Scalar bPlus = 0.26;
            // eddy viscosity can only be calculated correctly for non-zero distance to walls
            if (distanceToWallRough_ > 0)
                mixingLength_= karmanConstant_ * distanceToWallRough_ * (1 - std::exp( -1 * yPlusRough_ / aPlus ))
                    / sqrt(1 - std::exp( -1 * yPlusRough_ * bPlus ));
            else
                mixingLength_ = 0;

            dynamicEddyViscosity_ = this->density() * mixingLength() * mixingLength() * std::abs(velGrad_);
        }

//        Dune::dinfo
//            << "normVel " << this->normalVelocity()
//            << "   eddyVisc " << eddyViscosity()
//            << "   pos " << globalPos_
//            << "   dist2WReal " << distanceToWallReal_
//            << "   dist2WRough " << distanceToWallRough_
//            << "   bboxMin "<< problem.bBoxMin()
//            << "   bboxMax " << problem.bBoxMax()
//            << std::endl;
    }

public:

    /*!
     * \brief Returns the mixing length \f$\mathrm{[m]}\f$ for the element center.
     */
    Scalar mixingLength() const
    { return mixingLength_; }

    /*!
     * \brief Return the dynamic eddy viscosity 
     *        \f$\mathrm{[Pa \cdot s]} = \mathrm{[N \cdot s/m^2]}\f$.
     */
    const Scalar dynamicEddyViscosity() const
    { return dynamicEddyViscosity_; }

    /*!
     * \brief Return the kinematic eddy viscosity 
     *        \f$\mathrm{[m^2/s]}\f$ (if implemented).
     */
    const Scalar kinematicEddyViscosity() const
    { return dynamicEddyViscosity() / this->density(); }

    /*!
     * \brief Returns the inner dynamic eddy Viscosity (only Baldwin-Lomax model)
     *        \f$\mu_\textrm{inner}\f$ in \f$\mathrm{[N\cdot s/m^2]}\f$.
     */
    Scalar viscosityInner() const
    { return viscosityInner_; }

    /*!
     * \brief Returns the outer dynamic eddy Viscosity (only Baldwin-Lomax model).
     *        \f$\mu_\textrm{outer}\f$ in \f$\mathrm{[N\cdot s/m^2]}\f$.
     */
    Scalar viscosityOuter() const
    { return viscosityOuter_; }

    /*!
     * \brief Returns the value of the f-function.
     *
     * \f$\mathrm{f = y \omega \left( 1 - exp \left[ -y^+ / A^+ \right] \right)}\f$.<br>
     * y = distanceToWall
     */
    Scalar fz() const
    { return fz_; }

    /*!
     * \brief Returns the global Position.
     */
    DimVector globalPos() const
    { return globalPos_; }

    /*!
     * \brief Returns the distance to the corresponding Wall, including surface roughness \f$\mathrm{[m]}\f$.
     */
    Scalar distanceToWallRough() const
    { return distanceToWallRough_; }

    /*!
     * \brief Returns the real distance to the corresponding Wall \f$\mathrm{[m]}\f$.
     */
    Scalar distanceToWallReal() const
    { return distanceToWallReal_; }

    /*!
     * \brief Returns density at wall \f$\mathrm{[kg/m^3]}\f$.
     */
    Scalar densityWall() const
    { return densityWall_; }

    /*!
     * \brief Returns shear stress at wall \f$\mathrm{[m/s)]}\f$.
     */
    Scalar shearStressWall() const
    { return shearStressWall_; }

    /*!
     * \brief Returns friction velocity at wall \f$\mathrm{[kg/(m*s^2)]}\f$.
     */
    Scalar frictionVelocityWall() const
    { return frictionVelocityWall_; }

    /*!
     * \brief Returns real dimensionless wall distance \f$\mathrm{[-]}\f$.
     */
    Scalar yPlusReal() const
    { return yPlusReal_; }

    /*!
     * \brief Returns dimensionless wall distance, including surface roughness \f$\mathrm{[-]}\f$.
     */
    Scalar yPlusRough() const
    { return yPlusRough_; }

    /*!
     * \brief Returns the Karman constant \f$\mathrm{[-]}\f$.
     */
    Scalar karmanConstant() const
    { return karmanConstant_; }

    /*!
     * \brief Returns the name of the used turbulence Model.
     *
     * 0 = no eddy model<br>
     * 1 = Prandtl<br>
     * 2 = Deissler<br>
     * 3 = Michel<br>
     * 4 = Baldwin-Lomax<br>
     * 5 = Van Driest<br>
     * 6 = channel Flow<br>
     * 7 = Perrels and Karelse<br>
     * 8 = indirect model<br>
     * 9 = modified Van Driest<br>
     */
    const char *eddyViscosityModelName() const
    {
        if (eddyViscosityModel_ == 1)
            return "prandtl";
        else if (eddyViscosityModel_ == 2)
            return "deissler";
        else if (eddyViscosityModel_ == 3)
            return "michel";
        else if (eddyViscosityModel_ == 4)
            return "baldwin";
        else if (eddyViscosityModel_ == 5)
            return "vanDriest";
        else if (eddyViscosityModel_ == 6)
            return "channel";
        else if (eddyViscosityModel_ == 7)
            return "perrels";
        else if (eddyViscosityModel_ == 8)
            return "indirect";
        else if (eddyViscosityModel_ == 9)
            return "modVanDriest";
        else return "noModel";
    }

private:
    const int flowNormal_;
    const int wallNormal_;
    const int eddyViscosityModel_;
    const Scalar karmanConstant_;
    DimVector globalPos_;
    Scalar distanceToWallReal_;
    Scalar distanceToWallRough_;
    int wallIdx;
    int posIdx_;
    Scalar sandGrainRoughness_;
    Scalar sandGrainRoughnessDimensionless_;

    Scalar densityWall_;
    Scalar shearStressWall_;
    Scalar frictionVelocityWall_;
    Scalar yPlusReal_;
    Scalar yPlusRough_;

    Scalar mixingLength_;
    Scalar dynamicEddyViscosity_;
    Scalar viscosityInner_;
    Scalar viscosityOuter_;
    Scalar fz_;
    DimVector maxVelocity_;
    DimVector minVelocity_;
    Scalar velGrad_;
    Scalar runUpDistanceX_;
    Scalar runUpDistanceX2_;

    Scalar eps_;
};

} // end namespace

#endif // DUMUX_ZEROEQ_FLUX_VARIABLES_HH
