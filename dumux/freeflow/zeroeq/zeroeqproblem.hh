// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012-2013 by Thomas Fetzer                                *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for all ZeroEq problems which use an implicit method.
 *
 */
#ifndef DUMUX_ZEROEQ_PROBLEM_HH
#define DUMUX_ZEROEQ_PROBLEM_HH

#include <dumux/freeflow/stokes/stokesmodel.hh>
#include <dumux/freeflow/zeroeq/zeroeqproperties.hh>

namespace Dumux
{
/*!
 * \ingroup StokesModel
 * \ingroup ZeroEqModel
 * \brief Base class for all problems which use the ZeroEq model.
 *
 * This implements gravity (if desired) and a function returning the temperature.
 */
template<class TypeTag>
class ZeroEqProblem : public StokesProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef StokesProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GridView::Grid Grid;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;

    enum {
        dim = Grid::dimension,
        dimWorld = Grid::dimensionworld
    };

public:
    ZeroEqProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        lastTime_ = 0.0;
    }

    /*!
     * \brief Called by the Dumux::TimeManager in order to initialize the problem.
     *
     * If you overload this method don't forget to call ParentType::init()
     */
    void init()
    {
        // set the initial condition of the model
        ParentType::init();
        this->model().resetWallProperties();
        this->model().resetWallFluidProperties();
        if (GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMinSandGrainRoughness) > 0
            || GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMaxSandGrainRoughness) > 0)
        {
            Dune::dwarn << "INFO: surface roughness is not used for diffusivity and conductivity. " << std::endl;
            if (this->model().surfaceRoughnessNotImplemented())
            {
                Dune::dwarn << "INFO: surface roughness is not implemented for eddy viscosity model "
                            << GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel)
                            << "." << std::endl;
            }
        }
    }

    /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()
    {
        this->model().updateWallProperties(this->model().curSol());
    }

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

    Model model_;
    Scalar lastTime_;
};

}

#endif // DUMUX_ZEROEQ_PROBLEM_HH
