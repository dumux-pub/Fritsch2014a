// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012-2013 by Thomas Fetzer                                *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Adaptation of the box scheme to ZeroEq model.
 */
#ifndef DUMUX_ZEROEQ_MODEL_HH
#define DUMUX_ZEROEQ_MODEL_HH

#include <dumux/freeflow/stokes/stokesmodel.hh>

#include "zeroeqproperties.hh"
#include "zeroeqproblem.hh"
#include "zeroeqfluxvariables.hh"

namespace Dumux {
/*!
 * \ingroup StokesModel
 * \ingroup ZeroEqModel
 * \brief Adaptation of the box scheme to ZeroEq Navier Stokes model.
 *
 * This model implements an isothermal ZeroEq Navier Stokes flow (RANS) of a fluid
 * solving a momentum balance, a mass balance.
 *
 * \todo rename uPlus and yPlus into physical meaningful variables
 * \todo dynamic size of wallInterval struct
 *
 * \todo update balance equations
 * Momentum Balance (has to be updated):
 * \f[
 *   \frac{\partial \left(\varrho_g {\boldsymbol{v}}_g\right)}{\partial t}
 *   + \boldsymbol{\nabla} \boldsymbol{\cdot} \left(p_g {\bf {I}}
 *   - \mu_g \left(\boldsymbol{\nabla} \boldsymbol{v}_g
 *   + \boldsymbol{\nabla} \boldsymbol{v}_g^T\right)\right)
 *   - \varrho_g {\bf g} = 0,
 * \f]
 *
 * Mass balance (has to be updated):
 * \f[
 *  \frac{\partial \varrho_g}{\partial t} + \boldsymbol{\nabla}\boldsymbol{\cdot}\left(\varrho_g {\boldsymbol{v}}_g\right) - q_g = 0
 * \f]
 *
 * This is discretized using a fully-coupled vertex
 * centered finite volume (box) scheme as spatial and
 * the implicit Euler method as temporal discretization.
 */
template<class TypeTag>
class ZeroEqModel : public GET_PROP_TYPE(TypeTag, BaseStokesModel)
{
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        intervals = GET_PROP_VALUE(TypeTag, NumberOfIntervals),
        walls = GET_PROP_VALUE(TypeTag, NumberOfWalls),
        prec = 5, // precision of scv data
        width = prec + 7 // width of column
    };
    enum { numEq = GET_PROP_VALUE(TypeTag, NumEq) };

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> DimVector;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;


public:
    ZeroEqModel()
        : flowNormal_(GET_PARAM(TypeTag, int, FlowNormal))
        , wallNormal_(GET_PARAM(TypeTag, int, WallNormal))
        , bboxMaxIsWall_(GET_PARAM(TypeTag, bool, BBoxMaxIsWall))
        , bboxMinIsWall_(GET_PARAM(TypeTag, bool, BBoxMinIsWall))
    {
        eps_ = 1e-6;
        try
        {
            scvDataWriteAll_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, ZeroEq, SCVDataWriteAll);
            scvDataCoordinateDelta_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, SCVDataCoordinateDelta);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
             std::cerr << "Unknown exception thrown!\n";
             exit(1);
        }
    }

    /*!
     * \brief Calculate the fluxes across a certain layer in the domain.
     *        The layer is situated perpendicular to the coordinate axis "coord" and cuts
     *        the axis at the value "coordVal".
     *
     * \param globalSol The global solution vector.
     * \param flux A vector to store the flux.
     * \param axis The dimension, perpendicular to which the layer is situated.
     * \param coordVal The (Scalar) coordinate on the axis, at which the layer is situated.
     */
    void calculateFluxAcrossLayer(const SolutionVector &globalSol, Dune::FieldVector<Scalar, numEq> &flux, int axis, Scalar coordVal)
    {
        GlobalPosition globalI, globalJ;
        PrimaryVariables tmpFlux(0.0);
//         int sign;

        FVElementGeometry fvGeometry;
        ElementVolumeVariables elemVolVars;

        // Loop over elements
        ElementIterator elemIt = this->problem_.gridView().template begin<0>();
        ElementIterator endit = this->problem_.gridView().template end<0>();
        for (; elemIt != endit; ++elemIt)
        {
            if (elemIt->partitionType() != Dune::InteriorEntity)
                continue;

            fvGeometry.update(this->gridView_(), *elemIt);
            elemVolVars.update(this->problem_(), *elemIt, fvGeometry);
            this->localResidual().evalFluxes(*elemIt, elemVolVars);

            bool hasLeft = false;
            bool hasRight = false;
            for (int i = 0; i < fvGeometry.numVertices; i++) {
                const GlobalPosition &global = fvGeometry.subContVol[i].global;
                if (globalI[axis] < coordVal)
                    hasLeft = true;
                else if (globalI[axis] >= coordVal)
                    hasRight = true;
            }
            if (!hasLeft || !hasRight)
                continue;

            for (int i = 0; i < fvGeometry.numVertices; i++) {
//                 const int &globalIdx =
//                     this->vertexMapper().map(*elemIt, i, dim);
                const GlobalPosition &global = fvGeometry.subContVol[i].global;
                if (globalI[axis] < coordVal)
                    flux += this->localResidual().residual(i);
            }
        }

        flux = this->problem_.gridView().comm().sum(flux);
    }

    /*!
     * \brief Write vtk and additional plain text scv-data.
     *
     * The routine for the vtk data should be the same as in dumux-stable.
     *
     * The scv-data files contain mainly information for the turbulence model.
     *
     * \param sol The solution vector.
     * \param writer The writer for multi-file VTK datasets.
     */
    template <class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, dim> > VelocityField;

        // create the required scalar fields
        unsigned numVertices = this->gridView_().size(dim);
        ScalarField &pN = *writer.allocateManagedBuffer(numVertices);
        ScalarField &delP = *writer.allocateManagedBuffer(numVertices);
        ScalarField &rho = *writer.allocateManagedBuffer(numVertices);
        ScalarField &mu = *writer.allocateManagedBuffer(numVertices);
        VelocityField &velocity = *writer.template allocateManagedBuffer<Scalar, dim> (numVertices);

        unsigned numElements = this->gridView_().size(0);
        ScalarField &rank = *writer.allocateManagedBuffer(numElements);

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;

        ElementBoundaryTypes elemBcTypes;

        VertexIterator vIt = this->gridView_().template begin<dim> ();
        VertexIterator vEndIt = this->gridView_().template end<dim> ();
        for (; vIt != vEndIt; ++vIt)
        {}


        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            int idx = this->elementMapper().map(*elemIt);
            rank[idx] = this->gridView_().comm().rank();

            fvGeometry.update(this->gridView_(), *elemIt);
            elemBcTypes.update(this->problem_(), *elemIt, fvGeometry);


            int numLocalVerts = elemIt->template count<dim>();
            for (int i = 0; i < numLocalVerts; ++i)
            {
                int globalIdx = this->vertexMapper().map(*elemIt, i, dim);
                volVars.update(sol[globalIdx],
                               this->problem_(),
                               *elemIt,
                               fvGeometry,
                               i,
                               false);

                pN[globalIdx] = volVars.pressure();
                delP[globalIdx] = volVars.pressure() - 1e5;
                rho[globalIdx] = volVars.density();
                mu[globalIdx] = volVars.dynamicViscosity();
                velocity[globalIdx] = volVars.velocity();
            };
        }

        writer.attachVertexData(pN, "P");
        writer.attachVertexData(delP, "delP");
        writer.attachVertexData(rho, "rho");
        writer.attachVertexData(mu, "mu");
        writer.attachVertexData(velocity, "v", dim);


        // ensure that the actual values are given out
        asImp_().updateWallProperties(sol);

        elemIt = this->gridView_().template begin<0>();
        endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            fvGeometry.update(this->gridView_(), *elemIt);
            elemBcTypes.update(this->problem_(), *elemIt, fvGeometry);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(this->problem_(),
                               *elemIt,
                               fvGeometry,
                               false);

            IntersectionIterator isIt = this->gridView_().ibegin(*elemIt);
            const IntersectionIterator &endIt = this->gridView_().iend(*elemIt);

            for (; isIt != endIt; ++isIt)
            {
                int faceIdx = isIt->indexInInside();

                FluxVariables fluxVars(this->problem_(),
                                                    *elemIt,
                                                    fvGeometry,
                                                    faceIdx,
                                                    elemVolVars,
                                                    false);

                GlobalPosition globalPos = fvGeometry.subContVolFace[faceIdx].ipGlobal;

                if (asImp_().shouldWriteSCVData(globalPos))
                    asImp_().writeSCVData(volVars, fluxVars, globalPos);
            }
        }
    }


    /**********************************
     *
     * SCVDATA
     * -------
     *
     * This section contains functions to output zeroeq
     * related stuff.
     *
     */

    /*!
     * \brief Returns true if a scv-data file for gnuplot evaluation
     *        should be given out.
     *
     * \param global Global Position.
     */
    const bool shouldWriteSCVData (const GlobalPosition &global) const
    {
        Scalar delta = scvDataCoordinateDelta_;
        if (scvDataWriteAll_)
            return true;
        else
            return (
//                 (global[flowNormal_] > 0.01 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.01 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
//                 (global[flowNormal_] > 0.05 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.05 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
//                 (global[flowNormal_] > 0.09 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.09 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
                (global[flowNormal_] > 0.19 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.19 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
//                 (global[flowNormal_] > 0.29 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.29 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
                (global[flowNormal_] > 0.39 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.39 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
//                 (global[flowNormal_] > 0.49 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.49 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
                (global[flowNormal_] > 0.59 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.59 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
//                 (global[flowNormal_] > 0.69 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.69 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
                (global[flowNormal_] > 0.79 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.79 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
                (global[flowNormal_] > 0.89 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.89 * this->problem_().bBoxMax()[flowNormal_] + delta) ||
                (global[flowNormal_] > 0.99 * this->problem_().bBoxMax()[flowNormal_] - delta && global[flowNormal_] < 0.99 * this->problem_().bBoxMax()[flowNormal_] + delta)/* ||
                (global[flowNormal_] > 0.54 - delta * 10 && global[flowNormal_] < 0.54 + delta * 10) ||
                (global[flowNormal_] > 3.84 - delta * 10 && global[flowNormal_] < 3.84 + delta * 10) ||
                (global[flowNormal_] > 6.65 - delta * 10 && global[flowNormal_] < 6.65 + delta * 10)*/
                );
    }

    /*!
     * \brief Writes scv-data file for gnuplot evaluation.
     *
     * \param volVars Volume Variables of current scv.
     * \param fluxVars Flux Variables of current element.
     * \param global Global Position.
     */
    void writeSCVData(const VolumeVariables &volVars, const FluxVariables &fluxVars, const GlobalPosition &global)
    {
        std::ofstream fileAct ("");
        std::stringstream stream ("");

        char flowNormalChar = 'x';
        if (flowNormal_ == 1) flowNormalChar = 'y';
        stream << "scvData" << "-" << flowNormalChar << std::setprecision(4) << global[flowNormal_] / this->problem_().bBoxMax()[flowNormal_] << ".dat";
        std::string fileString2(stream.str());
        stream.str(""); stream.clear();
        const char* fileNameAct = fileString2.c_str();

        int posIdx = getPosIdx(global);
        int wallIdx = getWallIdx(global, posIdx);
        if (!wall[wallIdx].headerWritten[posIdx])
        {
            fileAct.open(fileNameAct, std::ios::out);

            asImp_().writeSCVHeader(stream, fluxVars, global);
            stream << std::endl;
            for (int wIdx = 0; wIdx < walls; ++wIdx)
            {
                asImp_().writeWallHeader(stream, posIdx, wIdx);
                stream << std::endl;
            }
            asImp_().writeDataHeader(stream, posIdx);
            stream << std::endl;

            fileAct << stream.str();
            stream.str(""); stream.clear();
            fileAct.close();
            for (int wIdx = 0; wIdx < walls; ++wIdx)
                wall[wIdx].headerWritten[posIdx] = true;
        }
        fileAct.open(fileNameAct, std::ios::app);

        asImp_().writeSCVDataValues(stream, volVars, fluxVars, global);
        stream << std::endl;
        fileAct << stream.str();
        fileAct.close();
    }

    /*!
     * \brief Writes scv-data header, containing basic properties, which are constant
     *        at this point.
     *
     * \param stream Output Filestream.
     * \param fluxVars Flux Variables of current element.
     * \param global Global Position.
     */
    void writeSCVHeader(std::stringstream &stream, const FluxVariables &fluxVars, const GlobalPosition &global)
    {
        int posIdx = getPosIdx(global);
        stream << "# over x=   " << std::setw(width) << std::setprecision(prec) << global[flowNormal_]
               << std::setw(width) << "at time" << std::setw(width) << std::setprecision(prec) << this->problem_().timeManager().time() << std::endl;
        stream << "# eddyVMd   " << std::setw(width) << GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel)
               << std::setw(2*width) << fluxVars.eddyViscosityModelName() << std::endl;
        stream << "# Re_d      " << std::setw(width) << std::setprecision(prec) << wall[0].maxVelocityAbs[posIdx][flowNormal_] * (this->problem_().bBoxMax()[wallNormal_] - this->problem_().bBoxMin()[wallNormal_]) / fluxVars.kinematicViscosity()
               << std::setw(width) << "Re_x" << std::setw(width) << std::setprecision(prec) << wall[0].maxVelocityAbs[posIdx][flowNormal_] * (this->problem_().bBoxMax()[flowNormal_] - this->problem_().bBoxMin()[flowNormal_]) / fluxVars.kinematicViscosity() << std::endl;
        stream << "# bl_tot    " << std::setw(width) << std::setprecision(prec) << wall[0].boundaryLayerThicknessCalculated[posIdx]
               << std::setw(width) << "vs_5" << std::setw(width) << std::setprecision(prec) << wall[0].viscousSublayerThicknessCalculated[posIdx]
               << std::setw(width) << "ks_+" << std::setw(width) << std::setprecision(prec) << wall[0].sandGrainRoughness[posIdx]
                                                                                               * std::sqrt(wall[0].wallVelGrad[posIdx] * wall[0].wallKinematicViscosity[posIdx])
                                                                                               / wall[0].wallKinematicViscosity[posIdx];
    }

    /*!
     * \brief Writes property names to wall-data header.
     *
     * \param stream Output Filestream.
     * \param posIdx Position Index of current Global Position.
     * \param wallIdx Wall Index of current Global Position.
     */
    void writeWallHeader(std::stringstream &stream, int posIdx, int wallIdx)
    {
        stream << "# wall[" << wallIdx << "]:" << "      nofIntvs" << std::setw(width) << intervals
            << std::setw(width) << "curIntPos" << std::setw(width) << posIdx
            << std::setw(width) << "minInt" << std::setw(width) << std::setprecision(prec) << this->problem_().bBoxMax()[flowNormal_] * (posIdx + 0) / intervals
            << std::setw(width) << "maxInt" << std::setw(width) << std::setprecision(prec) << this->problem_().bBoxMax()[flowNormal_] * (posIdx + 1) / intervals
            << std::setw(width) << "uMaxX" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].maxVelocity[posIdx][0]
            << std::setw(width) << "vMaxY" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].maxVelocity[posIdx][1]
            << std::setw(width) << "uMinX" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].minVelocity[posIdx][0]
            << std::setw(width) << "vMinY" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].minVelocity[posIdx][1]
            << std::setw(width) << "tau" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallShearStress[posIdx]
            << std::setw(width) << "density" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallDensity[posIdx]
            << std::setw(width) << "boundThi." << std::setw(width) << std::setprecision(prec) << wall[wallIdx].boundaryLayerThickness[posIdx]
            << std::setw(width) << "innL" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].crossLength[posIdx]
            << std::setw(width) << "fMax" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].fMax[posIdx]
            << std::setw(width) << "yMax" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].yMax[posIdx]
            << std::setw(width) << "noFluxVal" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].fluxValuesCount[posIdx]
            << std::setw(width) << "noWallVal" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallValuesCount[posIdx]
            << std::setw(width) << "isInterp" << std::setw(width) << std::setprecision(prec) << wall[wallIdx].isInterpolated[posIdx];
    }

    /*!
     * \brief Writes property names to scv-data header.
     *
     * \param stream Output Filestream.
     * \param posIdx Position Index of current Global Position.
     */
    void writeDataHeader(std::stringstream &stream, int posIdx)
    {

        stream << std::setw(1) << "#" << std::setw(width-1);
        for (int numIdx = 1; numIdx < 39; ++numIdx)
            stream << numIdx << std::setw(width);
        stream << std::endl;
        stream << std::setw(1) << "#" << std::setw(width-1) << "x"
            << std::setw(width) << "y"
            << std::setw(width) << "x/l"
            << std::setw(width) << "y/h"
            << std::setw(width) << "u=v_x"
            << std::setw(width) << "v=v_y"
            << std::setw(width) << "u/uMaxX"
            << std::setw(width) << "v/vMaxY"
            << std::setw(width) << "u_*"
            << std::setw(width) << "y^+"
            << std::setw(width) << "u^+"
            << std::setw(width) << "l_mix"
            << std::setw(width) << "velGrad"
            << std::setw(width) << "mu_turb"
            << std::setw(width) << "mu_lam"
            << std::setw(width) << "muInner"
            << std::setw(width) << "muOuter"
            << std::setw(width) << "fz"
            << std::setw(width) << "density"
            << std::setw(width) << "wallIdx"
            << std::setw(width) << "distTWRe"
            << std::setw(width) << "distTWRo"
            << std::setw(width) << "inBL"
            << std::setw(width) << "muInner";
    }

    /*!
     * \brief Writes values to scv-data.
     *
     * \param stream Output Filestream.
     * \param volVars Volume Variables of current scv.
     * \param fluxVars Flux Variables of current element.
     * \param globalPos Global Position.
     */
    void writeSCVDataValues(std::stringstream &stream, const VolumeVariables &volVars, const FluxVariables &fluxVars, const GlobalPosition &globalPos)
    {
    int posIdx = getPosIdx(globalPos);
    int wallIdx = getWallIdx(globalPos, posIdx);
    stream << std::setw(width) << std::setprecision(prec+1) << globalPos[0]
        << std::setw(width) << std::setprecision(prec+1) << globalPos[1]
        << std::setw(width) << std::setprecision(prec+1) << globalPos[0]/this->problem_().bBoxMax()[0]
        << std::setw(width) << std::setprecision(prec+1) << globalPos[1]/this->problem_().bBoxMax()[1]
        << std::setw(width) << std::setprecision(prec) << fluxVars.velocity()[0]
        << std::setw(width) << std::setprecision(prec) << fluxVars.velocity()[1]
        << std::setw(width) << std::setprecision(prec) << fluxVars.velocity()[0] / wall[wallIdx].maxVelocityAbs[posIdx][0] // maxVelocity[posIdx][0] // wall[wallIdx].maxVelocityAbs[posIdx][0]
        << std::setw(width) << std::setprecision(prec) << fluxVars.velocity()[1] / wall[wallIdx].maxVelocityAbs[posIdx][1] // maxVelocity[posIdx][1] // wall[wallIdx].maxVelocityAbs[posIdx][0]
        << std::setw(width) << std::setprecision(prec) << fluxVars.frictionVelocityWall()
        << std::setw(width) << std::setprecision(prec) << fluxVars.frictionVelocityWall() * std::abs(wall[wallIdx].wallPos[posIdx] - globalPos[wallNormal_]) / fluxVars.kinematicViscosity()
        << std::setw(width) << std::setprecision(prec) << fluxVars.velocity()[flowNormal_] / fluxVars.frictionVelocityWall()
        << std::setw(width) << std::setprecision(prec) << fluxVars.mixingLength()
        << std::setw(width) << std::setprecision(prec) << fluxVars.velocityGrad()[flowNormal_][wallNormal_]
        << std::setw(width) << std::setprecision(prec) << fluxVars.dynamicEddyViscosity()
        << std::setw(width) << std::setprecision(prec) << fluxVars.dynamicViscosity()
        << std::setw(width) << std::setprecision(prec) << fluxVars.viscosityInner()
        << std::setw(width) << std::setprecision(prec) << fluxVars.viscosityOuter()
        << std::setw(width) << std::setprecision(prec) << fluxVars.fz()
        << std::setw(width) << std::setprecision(prec) << fluxVars.density()
        << std::setw(width) << std::setprecision(prec) << wallIdx
        << std::setw(width) << std::setprecision(prec) << fluxVars.distanceToWallReal()
        << std::setw(width) << std::setprecision(prec) << fluxVars.distanceToWallRough()
        << std::setw(width) << std::setprecision(prec) << isInBoundaryLayer(globalPos)
        << std::setw(width) << std::setprecision(prec) << useViscosityInner(globalPos, posIdx);
    }





    /**********************************
     *
     * WALL PROPERTIES
     * ---------------
     *
     * This section contains functions concairning the treatment
     * of the wall (bounds) for viscous shear flow.
     *
     */

     /*!
     * \brief Container for all necessary information, needed to calculate them
     *        eddy viscosity at a certain point in relation to the wall distance
     *        and fluid/flow properties at the wall.
     *
     */
    struct WallProperties
    {
    public:
        bool isBBoxMinWall;                    //!< Actual wall properties are located on bboxmin or bboxmax.
        Scalar wallPos[intervals];             //!< Position of the wall interval in global coordinates.
        Scalar sandGrainRoughness[intervals];  //!< Sand grain roughness.
        Scalar boundaryLayerThickness[intervals];             //!< Domain influenced by this wall.
        Scalar boundaryLayerThicknessCalculated[intervals];   //!< Boundary layer thickness based on v = v_99.
        Scalar viscousSublayerThicknessCalculated[intervals]; //!< Viscous sublayer thickness based on y^+ <= 5.
        Scalar crossLength[intervals];         //!< Switching point for the Baldwin-Lomax model.
        Scalar maxVelocity[intervals][dim];    //!< Max velocity related to this wall.
        Scalar maxVelocityAbs[intervals][dim]; //!< Max velocity at this interval position.
        Scalar minVelocity[intervals][dim];    //!< Min velocity related to this wall.
        Scalar wallDensity[intervals];         //!< Fluid density at the wall.
        Scalar wallKinematicViscosity[intervals];             //!< Kinematic viscosity at the wall.
        Scalar wallVelGrad[intervals];         //!< Velocity gradient at the wall, in wall normal direction.
        Scalar wallShearStress[intervals];     //!< Shear stress at the wall.
        Scalar fMax[intervals];                //!< Max value of the f function for Baldwin-Lomax model.
        Scalar yMax[intervals];                //!< Distance of position where fMax occurs.
        Scalar maxMassFraction[intervals];     //!< Max mass fraction related to this wall.
        Scalar maxMoleFraction[intervals];     //!< Max mole fraction related to this wall.
        Scalar maxTemperature[intervals];      //!< Max temperature related to this wall.
        int isInterpolated[intervals];         //!< Value is only interpolated between neighboring wall intervals.
        int fluxValuesCount[intervals];        //!< Number of flux values contributing to the interval.
        int wallValuesCount[intervals];        //!< Number of values contributing properties directly at the wall.
        bool headerWritten[intervals];         //!< Header of scv-data file was already written.
        WallProperties() {}                    //!< Constructor for wall properties.
    };
    WallProperties wall[walls];

    /*!
     * \brief Initializes the wall structure with some standard values.
     *
     * This should only be done before updating (preTimeStepFunction and
     * only once per time Step), because the crossLength
     * for the BALDWIN LOMAX calculation is reset.
     */
    void resetWallProperties()
    {
        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
        {
            for (int posIdx = 0; posIdx < intervals; ++posIdx)
            {
                if (walls == 1)
                {
                    if (bboxMinIsWall_)
                    {
                        wall[wallIdx].wallPos[posIdx] = this->problem_().bBoxMin()[wallNormal_];
                        wall[wallIdx].boundaryLayerThickness[posIdx] = this->problem_().bBoxMax()[wallNormal_] - this->problem_().bBoxMin()[wallNormal_] + eps_;
                        wall[wallIdx].isBBoxMinWall = true;
                        wall[wallIdx].sandGrainRoughness[posIdx] = GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMinSandGrainRoughness);
                    }
                    if (bboxMaxIsWall_)
                    {
                        wall[wallIdx].wallPos[posIdx] = this->problem_().bBoxMax()[wallNormal_];
                        wall[wallIdx].boundaryLayerThickness[posIdx] = this->problem_().bBoxMin()[wallNormal_] - this->problem_().bBoxMax()[wallNormal_] - eps_;
                        wall[wallIdx].isBBoxMinWall = false;
                        wall[wallIdx].sandGrainRoughness[posIdx] = GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMaxSandGrainRoughness);
                    }
                }
                if (walls == 2 && wallIdx == 0)
                {
                    wall[0].wallPos[posIdx] = this->problem_().bBoxMin()[wallNormal_];
                    wall[1].wallPos[posIdx] = this->problem_().bBoxMax()[wallNormal_];
                    wall[0].boundaryLayerThickness[posIdx] = (wall[1].wallPos[posIdx] - wall[0].wallPos[posIdx]) / 2  + eps_;
                    wall[1].boundaryLayerThickness[posIdx] = - wall[0].boundaryLayerThickness[posIdx];
                    wall[0].isBBoxMinWall = true;
                    wall[1].isBBoxMinWall = false;
                    wall[0].sandGrainRoughness[posIdx] = GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMinSandGrainRoughness);
                    wall[1].sandGrainRoughness[posIdx] = GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMaxSandGrainRoughness);
                }
            wall[wallIdx].crossLength[posIdx] = wall[wallIdx].boundaryLayerThickness[posIdx];
            wall[wallIdx].boundaryLayerThicknessCalculated[posIdx] = wall[wallIdx].boundaryLayerThickness[posIdx]; // != 0, da kleinster wert gwünscht
            wall[wallIdx].viscousSublayerThicknessCalculated[posIdx] = 0; // == 0, da größter wert gewünscht
            for (int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                wall[wallIdx].maxVelocity[posIdx][dimIdx] = 0.0;
                wall[wallIdx].maxVelocityAbs[posIdx][dimIdx] = 0.0;
                wall[wallIdx].minVelocity[posIdx][dimIdx] = 0.0;
            }
            wall[wallIdx].fMax[posIdx] = 0.0;
            wall[wallIdx].yMax[posIdx] = 0.0;
            wall[wallIdx].isInterpolated[posIdx] = 0.0;
            wall[wallIdx].fluxValuesCount[posIdx] = 0.0;
            wall[wallIdx].wallValuesCount[posIdx] = 0.0;
            wall[wallIdx].headerWritten[posIdx] = false;
            }
        }
    }

    /*!
     * \brief Initializes the wall fluid properties with 0.
     */
    void resetWallFluidProperties()
    {
        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
            for (int posIdx = 0; posIdx < intervals; ++posIdx)
            {
                wall[wallIdx].wallDensity[posIdx] = 0.0;
                wall[wallIdx].wallKinematicViscosity[posIdx] = 0.0;
                wall[wallIdx].wallVelGrad[posIdx] = 0.0;
                wall[wallIdx].wallShearStress[posIdx] = 0.0;
            }
    }

    /*!
     * \brief Complete calculation for all relevant wall values.
     *
     * If the order is changed, errors may occur because of unknown are
     * zero valued values
     *
     * \param sol The global solution vector.
     */
    void updateWallProperties(const SolutionVector &sol)
    {
        asImp_().resetWallProperties();
        asImp_().updateMaxFluxVars();
        asImp_().updateCrossLength(sol);
        asImp_().resetWallFluidProperties();
        asImp_().updateWallFluidProperties(sol);
        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
        {
            for (int posIdx = 0; posIdx < intervals; ++posIdx)
                if (wall[wallIdx].viscousSublayerThicknessCalculated[posIdx] > wall[wallIdx].boundaryLayerThicknessCalculated[posIdx])
                    wall[wallIdx].viscousSublayerThicknessCalculated[posIdx] = wall[wallIdx].boundaryLayerThicknessCalculated[posIdx];
            asImp_().interpolateWallProperties(sol, wallIdx);
        }
        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
            asImp_().writeWallProperties(wallIdx);
    }

    /*!
     * \brief Get position index (interval section) a point belongs to.
     *
     * \param globalPos Global Position.
     */
    const int getPosIdx (const GlobalPosition &globalPos) const
    {
        int posIdx =
                int (intervals * (globalPos[flowNormal_] - this->problem_().bBoxMin()[flowNormal_]))
                / (this->problem_().bBoxMax()[flowNormal_] - this->problem_().bBoxMin()[flowNormal_]);
        if (posIdx >= intervals)
            posIdx = intervals -1;
        return posIdx;
    }

    /*!
     * \brief Return wall a point belongs to.
     *
     * \param posIdx Position Index of current Global Position.
     * \param globalPos Global Position.
     */
    const int getWallIdx (const GlobalPosition &globalPos, const int posIdx) const
    {
        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
            if ((wall[wallIdx].isBBoxMinWall && globalPos[wallNormal_] < wall[wallIdx].wallPos[posIdx] + wall[wallIdx].boundaryLayerThickness[posIdx])
                 || (!wall[wallIdx].isBBoxMinWall && globalPos[wallNormal_] > wall[wallIdx].wallPos[posIdx] + wall[wallIdx].boundaryLayerThickness[posIdx]))
            {
                return wallIdx;
            }

        static bool alreadyPrintedError = false;
        static Scalar timePrintedError = 0.0;
        if (this->problem_().timeManager().time() > timePrintedError)
        {
            alreadyPrintedError = false;
        }

        if (!alreadyPrintedError)
        {
            std::cout << "INFO: Point " << globalPos << " in Interval " << posIdx << " doesn't belong to wall -> now belongs to wall 0" << std::endl;
            alreadyPrintedError = true;
            timePrintedError = this->problem_().timeManager().time();
        }
        return 0;
    }


    /*!
     * \brief Return the distance to corresponding wall.
     *
     * \todo rename distanceToWallRough in distanceToWall (it works for ksPlus = 0.0, as well as ksPlus <> 0)
     * \param globalPos Global Position.
     * \param wallIdx Wall Index of current Global Position.
     * \param posIdx Position Index of current Global Position.
     */
    const Scalar distanceToWallRough (const GlobalPosition &globalPos, const int wallIdx, const int posIdx) const
    {
        if (surfaceRoughnessNotImplemented())
            { return distanceToWallReal (globalPos, wallIdx, posIdx); }
        if (GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMinSandGrainRoughness) < eps_&& wall[wallIdx].isBBoxMinWall)
            { return distanceToWallReal (globalPos, wallIdx, posIdx); }
        if (GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMaxSandGrainRoughness) < eps_&& !wall[wallIdx].isBBoxMinWall)
            { return distanceToWallReal (globalPos, wallIdx, posIdx); }

        Scalar ksPlus = wall[wallIdx].sandGrainRoughness[posIdx] * sqrt(wall[wallIdx].wallShearStress[posIdx] / wall[wallIdx].wallDensity[posIdx])
                       / wall[wallIdx].wallKinematicViscosity[posIdx];

        if (ksPlus > 2000)
        {
            Dune::dinfo << "Equivalent sand grain roughness ksPlus not in the valid range (ksPlus < 2000), but: " << ksPlus << " at " << globalPos << std::endl;
            Dune::dinfo << "Equivalent sand grain roughness ksPlus is set to maximum (ksPlus = 2000)" << std::endl;
            ksPlus = 2000;
        }
        else if (ksPlus < 4.535)
        {
            Dune::dinfo << "Equivalent sand grain roughness ksPlus not in the valid range (ksPlus > 4.535), but: " << ksPlus << " at " << globalPos << std::endl;
            Dune::dinfo << "Roughness length is set to to zero (delta y = 0)" << std::endl;
        }

        Scalar delta = 0.0; // delta is only changed for ksPlus, which are in valid range, otherwise smooth wall is assumed
        if (ksPlus >= 4.535)
        {
            delta = 0.9 * wall[wallIdx].wallKinematicViscosity[posIdx] / sqrt(wall[wallIdx].wallShearStress[posIdx] / wall[wallIdx].wallDensity[posIdx]) * (sqrt(ksPlus) - ksPlus * std::exp(- 1.0 * ksPlus / 6.0));
        }

        int sign = std::abs(globalPos[wallNormal_] - wall[wallIdx].wallPos[posIdx]) / (globalPos[wallNormal_] - wall[wallIdx].wallPos[posIdx]);
        return globalPos[wallNormal_] - wall[wallIdx].wallPos[posIdx] + sign * delta;
    }

    /*!
     * \brief Return the distance to corresponding wall.
     *
     * \param globalPos Global Position.
     * \param wallIdx Wall Index of current Global Position.
     * \param posIdx Position Index of current Global Position.
     */
    const Scalar distanceToWallReal (const GlobalPosition &globalPos, const int wallIdx, const int posIdx) const
    {   return globalPos[wallNormal_] - wall[wallIdx].wallPos[posIdx];  }

    /*!
     * \brief Return "true" if point is located inside the boundary layer.
     *
     * \param globalPos Global Position.
     */
    const bool isInBoundaryLayer (const GlobalPosition &globalPos) const
    {
        /* as we consider all points to be in the BL return is always true
        if extended to dynamic BL thickness give attention, that all turbulence
        related values are only evaluated if isInBoundaryLayer is true */
        return true;
    }

    /*!
     * \brief Return true if function for muInner should be used.
     *
     * \param globalPos Global Position.
     * \param posIdx Position Index of current Global Position.
     */
    const bool useViscosityInner (const GlobalPosition &globalPos, const int posIdx) const
    {
        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
            if ((wall[wallIdx].isBBoxMinWall && globalPos[wallNormal_] < wall[wallIdx].wallPos[posIdx] + wall[wallIdx].crossLength[posIdx])
                || (!wall[wallIdx].isBBoxMinWall && globalPos[wallNormal_] > wall[wallIdx].wallPos[posIdx] + wall[wallIdx].crossLength[posIdx]))
                return true;
        return false;
    }

    /*!
     * \brief Loop over all elements, used to calculate maximum flux values
     *        in the whole domain.
     */
    void updateMaxFluxVars()
    {
        FVElementGeometry fvGeometry;
        ElementBoundaryTypes elemBcTypes;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            fvGeometry.update(this->gridView_(), *elemIt);
            elemBcTypes.update(this->problem_(), *elemIt, fvGeometry);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(this->problem_(),
                               *elemIt,
                               fvGeometry,
                               false);

            for (int faceIdx = 0; faceIdx < fvGeometry.numScvf; ++faceIdx)
            {

                FluxVariables fluxVars(this->problem_(),
                                    *elemIt,
                                    fvGeometry,
                                    faceIdx,
                                    elemVolVars,
                                    false);

                GlobalPosition globalPos = fvGeometry.subContVolFace[faceIdx].ipGlobal;

                asImp_().calculateMaxFluxVars(fvGeometry, fluxVars, globalPos);
            }
        }
    }

    /*!
     * \brief Update maximum values in the domain and
     *        set them to the corresponding wall.
     *
     * \todo remove param fvGeometry.
     * \param fvGeometry currently unused.
     * \param fluxVars Flux Variables of current element.
     * \param globalPos Global Position.
     */
    void calculateMaxFluxVars(const FVElementGeometry &fvGeometry, const FluxVariables &fluxVars, const GlobalPosition globalPos)
    {
        int posIdx = getPosIdx(globalPos);
        int wallIdx = getWallIdx(globalPos, posIdx);
        for (int dimIdx = 0; dimIdx < dim; ++dimIdx)
        {
            if (std::abs(wall[wallIdx].maxVelocity[posIdx][dimIdx]) < std::abs(fluxVars.velocity()[dimIdx]))
            {
                wall[wallIdx].maxVelocity[posIdx][dimIdx] = fluxVars.velocity()[dimIdx];
    //                // if the values in the middle should be set on both wall
    //                for (int wIdx = 0; wIdx < walls; ++wIdx)
    //                    if (std::abs(distanceToWallReal(globalPos, wallIdx, posIdx)) < std::abs(wall[wIdx].boundaryLayerThickness[posIdx] + 1e-5))
    //                        wall[wIdx].maxVelocity[posIdx][dimIdx] = fluxVars.velocity()[dimIdx];
                // set it as maxVelocityAbs
                if (std::abs(wall[wallIdx].maxVelocityAbs[posIdx][dimIdx]) < std::abs(fluxVars.velocity()[dimIdx]))
                    for (int wIdx = 0; wIdx < walls; ++wIdx)
                        wall[wIdx].maxVelocityAbs[posIdx][dimIdx] = fluxVars.velocity()[dimIdx];
                wall[wallIdx].fluxValuesCount[posIdx]++;
            }
            if (std::abs(wall[wallIdx].minVelocity[posIdx][dimIdx]) > std::abs(fluxVars.velocity()[dimIdx]))
                wall[wallIdx].minVelocity[posIdx][dimIdx] = fluxVars.velocity()[dimIdx];
        }

        // fMax and yMax
        if (wall[wallIdx].fMax[posIdx] < fluxVars.fz())
        {
            wall[wallIdx].fMax[posIdx] = fluxVars.fz();
            wall[wallIdx].yMax[posIdx] = distanceToWallReal(globalPos, wallIdx, posIdx);
//            // if the values in the middle should be set on both wall
//            for (int wIdx = 0; wIdx < walls; ++wIdx)
//                if (std::abs(distanceToWall(globalPos, wIdx, posIdx)) < std::abs(wall[wIdx].boundaryLayerThickness[posIdx] + 1e-4))
//                    {
//                        wall[wIdx].fMax[posIdx] = fluxVars.fz();
//                        wall[wIdx].yMax[posIdx] = distanceToWall(globalPos, wallIdx, posIdx);
//                    }
        }
    }

    /*!
     * \brief Get maximum values of the f(z) function in the Baldwin-Lomax model.
     *
     * \param sol The global solution vector.
     */
    void updateCrossLength (const SolutionVector &sol)
    {
        FVElementGeometry fvGeometry;
        ElementBoundaryTypes elemBcTypes;
        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            fvGeometry.update(this->gridView_(), *elemIt);
            elemBcTypes.update(this->problem_(), *elemIt, fvGeometry);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(this->problem_(),
                               *elemIt,
                               fvGeometry,
                               false);

            IntersectionIterator isIt = this->gridView_().ibegin(*elemIt);
            const IntersectionIterator &endIt = this->gridView_().iend(*elemIt);

            for (; isIt != endIt; ++isIt)
            {
                int faceIdx = isIt->indexInInside();
//
//            for (int faceIdx = 0; faceIdx < fvGeometry.numEdges; ++faceIdx)
//            {
                FluxVariables fluxVars(this->problem_(),
                                                    *elemIt,
                                                    fvGeometry,
                                                    faceIdx,
                                                    elemVolVars,
                                                    false);

                GlobalPosition globalPos = fvGeometry.subContVolFace[faceIdx].ipGlobal;
                int posIdx = getPosIdx(globalPos);
                int wallIdx = getWallIdx(globalPos, posIdx);

                // muCross
                if (fluxVars.viscosityOuter() < fluxVars.viscosityInner() && useViscosityInner(globalPos, posIdx))
                    wall[wallIdx].crossLength[posIdx] = distanceToWallReal(globalPos, wallIdx, posIdx) - eps_ * std::abs(distanceToWallReal(globalPos, wallIdx, posIdx) / distanceToWallReal(globalPos, wallIdx, posIdx));

                if (std::abs(fluxVars.velocity()[flowNormal_]) >= 0.99 * std::abs(wall[wallIdx].maxVelocity[posIdx][flowNormal_])
                    && std::abs(wall[wallIdx].boundaryLayerThicknessCalculated[posIdx]) > std::abs(distanceToWallReal(globalPos, wallIdx, posIdx)))
                    wall[wallIdx].boundaryLayerThicknessCalculated[posIdx] = distanceToWallReal(globalPos, wallIdx, posIdx);

//                // boundary layer
//                if (std::abs(isIt->centerUnitOuterNormal()[flowNormal_])
//                    < std::abs(isIt->centerUnitOuterNormal()[wallNormal_]))
                {
                if (wall[wallIdx].maxVelocity[posIdx][flowNormal_] * globalPos[flowNormal_] / fluxVars.kinematicViscosity()
                        < 2300)
                    wall[wallIdx].viscousSublayerThicknessCalculated[posIdx] = wall[wallIdx].boundaryLayerThicknessCalculated[posIdx];
                else if (
                    (fluxVars.velocity()[flowNormal_] / fluxVars.frictionVelocityWall()
                        <= 5.0)
                    &&
                    (std::abs(wall[wallIdx].viscousSublayerThicknessCalculated[posIdx])
                        < std::abs(distanceToWallReal(globalPos, wallIdx, posIdx)))
                   )
                    wall[wallIdx].viscousSublayerThicknessCalculated[posIdx] = distanceToWallReal(globalPos, wallIdx, posIdx);
                }
            }
        }
    }

    /*!
     * \brief Loop over all elements to update the values at the wall.
     *
     * \param sol The global solution vector.
     */
    void updateWallFluidProperties(const SolutionVector &sol)
    {
        FVElementGeometry fvGeometry;
        ElementBoundaryTypes elemBcTypes;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            fvGeometry.update(this->gridView_(), *elemIt);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(this->problem_(),
                               *elemIt,
                               fvGeometry,
                               false);

            const ReferenceElement &refElement = ReferenceElements::general(elemIt->geometry().type());
            IntersectionIterator isIt = this->gridView_().ibegin(*elemIt);
            const IntersectionIterator &endIt = this->gridView_().iend(*elemIt);
            for (; isIt != endIt; ++isIt)
            {
                // handle only faces on the boundary
                if (!isIt->boundary())
                    continue;

                // Assemble the boundary for all vertices of the current
                // face
                int faceIdx = isIt->indexInInside();
                int numFaceVerts = refElement.size(faceIdx, 1, dim);
                for (int faceVertIdx = 0;
                     faceVertIdx < numFaceVerts;
                     ++faceVertIdx)
                {
                    int boundaryFaceIdx = fvGeometry.boundaryFaceIndex(faceIdx, faceVertIdx);




                    const FluxVariables boundaryVars(this->problem_(),
                                                     *elemIt,
                                                     fvGeometry,
                                                     boundaryFaceIdx,
                                                     elemVolVars,
                                                     true);

                    GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
                    if (
                        globalPos[wallNormal_] > this->problem_().bBoxMin()[wallNormal_] + 1e-15
                        && globalPos[wallNormal_] < this->problem_().bBoxMax()[wallNormal_] - 1e-15
                        )
                    continue;

                    asImp_().calculateWallFluidProperties(fvGeometry, boundaryVars, globalPos);
                } // end loop over intersections
            } // end loop over element vertices
        }
    }

    /*!
     * \brief Calculate / average the values at the wall.
     *
     * \todo remove fvGeometry
     * \param fvGeometry currently unused
     * \param boundaryVars Flux Variables at boundary segment.
     * \param globalPos Global Position.
     */
    void calculateWallFluidProperties(const FVElementGeometry &fvGeometry, const FluxVariables &boundaryVars, const GlobalPosition &globalPos)
    {
        int posIdx = getPosIdx(globalPos);
        int wallIdx = getWallIdx(globalPos, posIdx);
        wall[wallIdx].wallValuesCount[posIdx] += 1;
        wall[wallIdx].wallDensity[posIdx] =
            (wall[wallIdx].wallDensity[posIdx] * (wall[wallIdx].wallValuesCount[posIdx] - 1) + boundaryVars.density())
            / wall[wallIdx].wallValuesCount[posIdx];
        wall[wallIdx].wallKinematicViscosity[posIdx] =
            (wall[wallIdx].wallKinematicViscosity[posIdx] * (wall[wallIdx].wallValuesCount[posIdx] - 1) + boundaryVars.kinematicViscosity())
            / wall[wallIdx].wallValuesCount[posIdx];
        wall[wallIdx].wallVelGrad[posIdx] =
            (boundaryVars.velocityGrad()[flowNormal_][wallNormal_] * (wall[wallIdx].wallValuesCount[posIdx] - 1) + boundaryVars.velocityGrad()[flowNormal_][wallNormal_])
            / wall[wallIdx].wallValuesCount[posIdx];
        wall[wallIdx].wallShearStress[posIdx] =
            (wall[wallIdx].wallShearStress[posIdx] * (wall[wallIdx].wallValuesCount[posIdx] - 1)
                + std::abs(boundaryVars.velocityGrad()[flowNormal_][wallNormal_]) * boundaryVars.dynamicViscosity())
            / wall[wallIdx].wallValuesCount[posIdx];
    }


    /*!
     * \brief Find points with given values and start interpolation.
     *
     * \param sol The global solution vector.
     * \param wallIdx Wall Index of current Global Position.
     */
    const void interpolateWallProperties (const SolutionVector &sol, const int wallIdx)
    {
        const int startInterpolation = 0;
        const int endInterpolation = intervals;

        for (int posIdx = startInterpolation; posIdx < endInterpolation; ++posIdx)
        {
            if (wall[wallIdx].fluxValuesCount[posIdx] == 0)
            {
                int prevIdx = posIdx;
                int nextIdx = posIdx;
                // Getting previous value, if 0 is reached (and tau is still < eps_), get next value
                for (prevIdx = posIdx-1; prevIdx >= startInterpolation; --prevIdx)
                    if (wall[wallIdx].fluxValuesCount[prevIdx] != 0)
                        break;
                if (prevIdx < startInterpolation) // interpolation at x=0, prevIdx is -1
                    for (prevIdx = posIdx+1; prevIdx < endInterpolation; ++prevIdx)
                        if (wall[wallIdx].fluxValuesCount[prevIdx] != 0)
                            break;
                if (prevIdx == endInterpolation && this->problem_().timeManager().time() > this->problem_().timeManager().timeStepSize())
                {
                    std::cout << "For posIdx " << posIdx << "on wall " << wallIdx << "   no   fluxValue   interpolation!!" << std::endl;
                    break;
                }

                // Getting next value, if intervals is reached get prev value
                for (nextIdx = posIdx+1; nextIdx < endInterpolation; ++nextIdx)
                    if (wall[wallIdx].fluxValuesCount[nextIdx] != 0 && nextIdx != prevIdx)
                        break;
                if (nextIdx == endInterpolation)
                    for (nextIdx = posIdx-1; nextIdx >= startInterpolation; --nextIdx)
                        if (wall[wallIdx].fluxValuesCount[nextIdx] != 0 && nextIdx != prevIdx)
                            break;

                asImp_().doInterpolationFluxValues(wallIdx, posIdx, prevIdx, nextIdx);
            }


            if (wall[wallIdx].wallValuesCount[posIdx] == 0) // || wall[wallIdx].wallValuesCount[posIdx] > 50)
            {
                int prevIdx = posIdx;
                int nextIdx = posIdx;
                // Getting previous value, if 0 is reached (and tau is still < eps_), get next value
                for (prevIdx = posIdx-1; prevIdx >= startInterpolation; --prevIdx)
                    if (wall[wallIdx].wallValuesCount[prevIdx] != 0)
                        break;
                if (prevIdx < startInterpolation) // interpolation at x=0, prevIdx is -1
                    for (prevIdx = posIdx+1; prevIdx < endInterpolation; ++prevIdx)
                        if (wall[wallIdx].wallValuesCount[prevIdx] != 0)
                            break;
                if (prevIdx == endInterpolation && this->problem_().timeManager().time() > this->problem_().timeManager().timeStepSize())
                {
                    std::cout << "For posIdx " << posIdx << "on wall " << wallIdx << "   no wallValue  interpolation!!" << std::endl;
                    break;
                }

                // Getting next value, if intervals is reached get prev value
                for (nextIdx = posIdx+1; nextIdx < endInterpolation; ++nextIdx)
                    if (wall[wallIdx].wallValuesCount[nextIdx] != 0 && nextIdx != prevIdx)
                        break;
                if (nextIdx == endInterpolation)
                    for (nextIdx = posIdx-1; nextIdx >= startInterpolation; --nextIdx)
                        if (wall[wallIdx].wallValuesCount[nextIdx] != 0 && nextIdx != prevIdx)
                            break;

                asImp_().doInterpolationWallValues(wallIdx, posIdx, prevIdx, nextIdx);
            }
        }
    }

    /*!
     * \brief Interpolate flux Values, so that flux related Properties
     *        are given in every Interval.
     *
     * \param wallIdx Wall Index for interpolation.
     * \param posIdx Position Index for interpolation (no given value).
     * \param prevIdx Position Index with value.
     * \param nextIdx Position Index with value.
     */
    const void doInterpolationFluxValues (const int wallIdx, const int posIdx, const int prevIdx, const int nextIdx)
    {
        wall[wallIdx].boundaryLayerThickness[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].boundaryLayerThickness[prevIdx], nextIdx, wall[wallIdx].boundaryLayerThickness[nextIdx]);
        wall[wallIdx].boundaryLayerThicknessCalculated[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].boundaryLayerThicknessCalculated[prevIdx], nextIdx, wall[wallIdx].boundaryLayerThicknessCalculated[nextIdx]);
        wall[wallIdx].viscousSublayerThicknessCalculated[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].viscousSublayerThicknessCalculated[prevIdx], nextIdx, wall[wallIdx].viscousSublayerThicknessCalculated[nextIdx]);
        wall[wallIdx].crossLength[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].crossLength[prevIdx], nextIdx, wall[wallIdx].crossLength[nextIdx]);
        wall[wallIdx].maxVelocity[posIdx][0] = interpolation(posIdx, prevIdx, wall[wallIdx].maxVelocity[prevIdx][0], nextIdx, wall[wallIdx].maxVelocity[nextIdx][0]);
        wall[wallIdx].maxVelocity[posIdx][1] = interpolation(posIdx, prevIdx, wall[wallIdx].maxVelocity[prevIdx][1], nextIdx, wall[wallIdx].maxVelocity[nextIdx][1]);
        wall[wallIdx].minVelocity[posIdx][0] = interpolation(posIdx, prevIdx, wall[wallIdx].minVelocity[prevIdx][0], nextIdx, wall[wallIdx].minVelocity[nextIdx][0]);
        wall[wallIdx].minVelocity[posIdx][1] = interpolation(posIdx, prevIdx, wall[wallIdx].minVelocity[prevIdx][1], nextIdx, wall[wallIdx].minVelocity[nextIdx][1]);
        wall[wallIdx].maxVelocityAbs[posIdx][0] = interpolation(posIdx, prevIdx, wall[wallIdx].maxVelocityAbs[prevIdx][0], nextIdx, wall[wallIdx].maxVelocityAbs[nextIdx][0]);
        wall[wallIdx].maxVelocityAbs[posIdx][1] = interpolation(posIdx, prevIdx, wall[wallIdx].maxVelocityAbs[prevIdx][1], nextIdx, wall[wallIdx].maxVelocityAbs[nextIdx][1]);
        wall[wallIdx].fMax[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].fMax[prevIdx], nextIdx, wall[wallIdx].fMax[nextIdx]);
        wall[wallIdx].yMax[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].yMax[prevIdx], nextIdx, wall[wallIdx].yMax[nextIdx]);
        wall[wallIdx].isInterpolated[posIdx] += 1;
    }

    /*!
     * \brief Interpolate wall Values, so that wall related Properties
     *        are given in every Interval.
     *
     * \param wallIdx Wall Index for interpolation.
     * \param posIdx Position Index for interpolation (no given value).
     * \param prevIdx Position Index with value.
     * \param nextIdx Position Index with value.
     */
    const void doInterpolationWallValues (const int wallIdx, const int posIdx, const int prevIdx, const int nextIdx)
    {
        wall[wallIdx].wallDensity[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].wallDensity[prevIdx], nextIdx, wall[wallIdx].wallDensity[nextIdx]);
        wall[wallIdx].wallKinematicViscosity[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].wallKinematicViscosity[prevIdx], nextIdx, wall[wallIdx].wallKinematicViscosity[nextIdx]);
        wall[wallIdx].wallVelGrad[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].wallVelGrad[prevIdx], nextIdx, wall[wallIdx].wallVelGrad[nextIdx]);
        wall[wallIdx].wallShearStress[posIdx] = interpolation(posIdx, prevIdx, wall[wallIdx].wallShearStress[prevIdx], nextIdx, wall[wallIdx].wallShearStress[nextIdx]);
        wall[wallIdx].isInterpolated[posIdx] += 2;
    }

    /*!
     * \brief Mathematical linear interpolation routine.
     *
     * Return the linear interpolated value at any point between to values.
     *
     * \param position Position for which interpolation is made.
     * \param prev First Position for which the value is known.
     * \param prevValue Known value at prev position.
     * \param next Second Position for which the value is known.
     * \param nextValue Known value at next position.
     */
    const Scalar interpolation (const Scalar position, const Scalar prev, const Scalar prevValue, const Scalar next, const Scalar nextValue)
    {
        return (prevValue + (nextValue - prevValue) / (next - prev) * (position - prev));
    }


    /*!
     * \brief Creates a data files with all relevant wall properties data file.
     *
     * Use this functions to detect and erases errors.
     *
     * \param wallIdx Wall Index of current Global Position.
     */
    void writeWallProperties(int wallIdx)
    {
        std::ofstream fileAct;
        std::stringstream stream;

        stream << "wallProperties" << wallIdx << ".dat";
        std::string fileString(stream.str());
        stream.str(""); stream.clear();
        const char* fileNameAct = fileString.c_str();

        fileAct.open(fileNameAct, std::ios::out);
        stream << "#   wall " << wallIdx << "         isBBoxMin " << wall[wallIdx].isBBoxMinWall << "  nofIntvs" << std::setw(width) << intervals << std::endl;
        stream << "#" << std::setw(width-1);

        fileAct << stream.str();
        stream.str(""); stream.clear();
        fileAct.close();
        fileAct.open(fileNameAct, std::ios::app);

        for (int numIdx = 1; numIdx < 23; ++numIdx)
            stream << numIdx << std::setw(width);

        stream << std::endl;
        asImp_().writeWallPropertiesHeader(stream);
        stream << std::endl;

        for (int posIdx = 0; posIdx < intervals; ++posIdx)
        {
            asImp_().writeWallPropertiesValues(stream, wallIdx, posIdx);
            stream << std::endl;
            fileAct << stream.str();
            stream.str(""); stream.clear();
        }
        fileAct.close();
    }


    /*!
     * \brief Writes a header to the wall proprties data file.
     *
     * \param stream Output Filestream.
     */
    void writeWallPropertiesHeader(std::stringstream &stream)
    {
        stream << std::setw(1) << "#" << std::setw(width-1) << "posIdx" << std::setw(width)
            << std::setw(width) << "posIdx*" << std::setw(width)
            << std::setw(width) << "minInt" << std::setw(width)
            << std::setw(width) << "maxInt" << std::setw(width)
            << std::setw(width) << "uMaxX" << std::setw(width)
            << std::setw(width) << "vMaxY" << std::setw(width)
            << std::setw(width) << "vMaxXAbs" << std::setw(width)
            << std::setw(width) << "vMaxYAbs" << std::setw(width)
            << std::setw(width) << "uMinX" << std::setw(width)
            << std::setw(width) << "vMinY" << std::setw(width)
            << std::setw(width) << "density" << std::setw(width)
            << std::setw(width) << "velGrad" << std::setw(width)
            << std::setw(width) << "tau" << std::setw(width)
            << std::setw(width) << "ks" << std::setw(width)
//          << std::setw(width) << "ksPlus" << std::setw(width)
            << std::setw(width) << "boundThi." << std::setw(width)
            << std::setw(width) << "blThCalc" << std::setw(width)
            << std::setw(width) << "vsThCalc" << std::setw(width)
            << std::setw(width) << "innL" << std::setw(width)
            << std::setw(width) << "fMax" << std::setw(width)
            << std::setw(width) << "yMax" << std::setw(width)
            << std::setw(width) << "#FluxVal" << std::setw(width)
            << std::setw(width) << "#WallVal" << std::setw(width)
            << std::setw(width) << "interpol" << std::setw(width);
    }

    /*!
     * \brief Writes a wall proprties to the data file.
     *
     * \param wallIdx Wall Index of current Global Position.
     * \param posIdx Position Index of current Global Position.
     * \param stream Output Filestream.
     */
    void writeWallPropertiesValues(std::stringstream &stream, int wallIdx, int posIdx)
    {
        stream << std::setw(width) << std::setprecision(prec) << posIdx
            << std::setw(width) << std::setprecision(prec) << (posIdx + .5) / intervals
            << std::setw(width) << std::setprecision(prec) << this->problem_().bBoxMax()[flowNormal_] * (posIdx + 0) / intervals + this->problem_().bBoxMin()[flowNormal_]
            << std::setw(width) << std::setprecision(prec) << this->problem_().bBoxMax()[flowNormal_] * (posIdx + 1) / intervals + this->problem_().bBoxMin()[flowNormal_]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].maxVelocity[posIdx][0]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].maxVelocity[posIdx][1]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].maxVelocityAbs[posIdx][0]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].maxVelocityAbs[posIdx][1]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].minVelocity[posIdx][0]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].minVelocity[posIdx][1]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallDensity[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallVelGrad[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallShearStress[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].sandGrainRoughness[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].boundaryLayerThickness[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].boundaryLayerThicknessCalculated[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].viscousSublayerThicknessCalculated[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].crossLength[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].fMax[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].yMax[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].fluxValuesCount[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].wallValuesCount[posIdx]
            << std::setw(width) << std::setprecision(prec) << wall[wallIdx].isInterpolated[posIdx];
    }

    /*!
     * \brief Returns whether the actual eddy viscosity model includes surface roughness approach.
     *
     * Surface roughness is NOT included in the following models:<br>
     * EddyViscosityModel = 0, no model<br>
     * EddyViscosityModel = 4, Baldwin-Lomax<br>
     * EddyViscosityModel = 7, Perrels-Karrelse (surface roughnessis already included in approach)<br>
     * EddyViscosityModel = 8, indirect model
     */
    const bool surfaceRoughnessNotImplemented() const
    {
        return (
                GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel) == 0 || // no model
                GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel) == 4 || // baldwin-lomax
                GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel) == 7 || // perrels-karrelse
                GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel) == 8 // indirect
               );
    }


protected:
    //! Current implementation.
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
    //! Current implementation.
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }

private:
    const int flowNormal_;
    const int wallNormal_;
    const bool bboxMaxIsWall_;
    const bool bboxMinIsWall_;
    bool scvDataWriteAll_;
    Scalar scvDataCoordinateDelta_;
    Scalar eps_;

};

}

#include "zeroeqpropertydefaults.hh"

#endif // DUMUX_ZEROEQ_MODEL_HH
