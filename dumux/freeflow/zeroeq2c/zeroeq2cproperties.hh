// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Thomas Fetzer                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesncModel
 * \ingroup BoxZeroEq2cModel
 *
 * \file
 *
 * \brief Defines the properties required for the
 *        compositional ZeroEq box model.
 *
 */

#ifndef DUMUX_FREEFLOW_ZEROEQ2C_PROPERTIES_HH
#define DUMUX_FREEFLOW_ZEROEQ2C_PROPERTIES_HH

#include <dumux/freeflow/stokesnc/stokesncproperties.hh>
#include <dumux/freeflow/zeroeq/zeroeqproperties.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the ZeroEq Problem
NEW_TYPE_TAG(BoxZeroEq2c, INHERITS_FROM(BoxZeroEq, BoxStokesnc));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(ZeroEqEddyDiffusivityModel); //!< Returns which Eddy Diffusivity Model is used
}
}

#endif // DUMUX_FREEFLOW_ZEROEQ2C_PROPERTIES_HH
