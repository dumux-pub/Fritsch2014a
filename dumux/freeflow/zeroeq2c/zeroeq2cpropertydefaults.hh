// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Thomas Fetzer                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesncModel
 * \ingroup BoxZeroEq2cModel
 *
 * \file
 *
 * \brief Defines default properties for the
 *        compositional ZeroEq box model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 *
 */

#ifndef DUMUX_ZEROEQ2C_PROPERTY_DEFAULTS_HH
#define DUMUX_ZEROEQ2C_PROPERTY_DEFAULTS_HH

#include <dumux/freeflow/stokesnc/stokesncfluxvariables.hh>
#include <dumux/freeflow/stokesnc/stokesncmodel.hh>
#include <dumux/freeflow/stokesnc/stokesncvolumevariables.hh>
#include "zeroeq2cfluxvariables.hh"
#include "zeroeq2cmodel.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////


// INHERITED
//! Set the Model property
SET_TYPE_PROP(BoxZeroEq2c, Model, ZeroEq2cModel<TypeTag>);

//! Set the FluxVariables property
SET_TYPE_PROP(BoxZeroEq2c, FluxVariables, ZeroEq2cFluxVariables<TypeTag>);

//! Set calculation to Navier-Stokes
SET_BOOL_PROP(BoxZeroEq2c, EnableNavierStokes, true);

//! Don't use mole fractions
SET_BOOL_PROP(BoxZeroEq2c, UseMoles, false);

// NEW PROPERTIES
//! Set type of Eddy Diffusivity Model
SET_INT_PROP(BoxZeroEq2c, ZeroEqEddyDiffusivityModel, 1);

//! Set the BaseStokesModel to StokesncModel
SET_TYPE_PROP(BoxZeroEq2c, BaseStokesModel, StokesncModel<TypeTag>);

//! Set the BaseStokesFluxVariables to StokesncniFluxVariables
SET_TYPE_PROP(BoxZeroEq2c, BaseStokesFluxVariables, StokesncFluxVariables<TypeTag>);

//! Set the BaseStokesVolumeVariables to StokesncVolumeVariables
SET_TYPE_PROP(BoxZeroEq2c, BaseStokesVolumeVariables, StokesncVolumeVariables<TypeTag>);
}
}

#endif // DUMUX_ZEROEQ2C_PROPERTY_DEFAULTS_HH
