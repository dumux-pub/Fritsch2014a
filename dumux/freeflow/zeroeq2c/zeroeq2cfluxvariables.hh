// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Thomas Fetzer                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief This file contains the data which is required to calculate
 *        the fluxes of the
 *        compositional ZeroEq model
 *        over a face of a finite volume.
 *
 * This means pressure gradients, phase densities at the integration point, etc.
 *
 */
#ifndef DUMUX_ZEROEQ2C_FLUX_VARIABLES_HH
#define DUMUX_ZEROEQ2C_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/common/valgrind.hh>

#include <dumux/freeflow/zeroeq/zeroeqfluxvariables.hh>
#include <dumux/freeflow/zeroeq2c/zeroeq2cproperties.hh>

namespace Dumux
{

/*!
 * \ingroup BoxStokesncModel
 * \ingroup BoxZeroEq2cModel
 * \brief This template class contains the data which is required to
 *        calculate the mass and momentum fluxes over the face of a
 *        sub-control volume for the
 *        compositional ZeroEq model
 *        over a face of a finite volume.
 *
 * This means pressure gradients, phase densities, viscosities, etc.
 * at the integration point of the sub-control-volume face
 */
template <class TypeTag>
class ZeroEq2cFluxVariables : public ZeroEqFluxVariables<TypeTag>
{
    typedef ZeroEqFluxVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension,
        phaseIdx = Indices::phaseIdx,
        transportCompIdx = Indices::transportCompIdx
        };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

public:
    ZeroEq2cFluxVariables(const Problem &problem,
                        const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int faceIdx,
                        const ElementVolumeVariables &elemVolVars,
                        const bool onBoundary = false)
        : ParentType(problem, element, fvGeometry, faceIdx, elemVolVars, onBoundary)
        , flowNormal_(GET_PARAM(TypeTag, int, FlowNormal))
        , wallNormal_(GET_PARAM(TypeTag, int, WallNormal))
        , eddyDiffusivityModel_(GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyDiffusivityModel))
    {
        globalPos_ = this->globalPos();
        posIdx_ = problem.model().getPosIdx(globalPos_);
        wallIdx_ = problem.model().getWallIdx(globalPos_, posIdx_);
        velGrad_ = this->velocityGrad_[flowNormal_][wallNormal_];
        velGradWall_ = problem.model().wall[wallIdx_].wallVelGrad[posIdx_];
        eddyDiffusivity_ = Scalar(0);
        mixingLengthDiffusivity_ = Scalar(0);
        richardsonNumberComponent_ = 0.0;
        schmidtNumber_ = 0.0;

        if (GET_PARAM(TypeTag, bool, EnableNavierStokes))   // calculation of an eddy diffusivity only makes sense WITH navier stokes term
            calculateEddyDiffusivity_(problem, element, elemVolVars);
    };

protected:
    /*!
    * \brief This functions calculates a eddy diffusivity (which is added
    *        to the diffusivity in stokesnclocalresidual.hh) at each scv
    *        face.
    *
    * \todo at the moment, there is only one eddy diffusivity for all components
    *       maybe this has to be revised
    */
    void calculateEddyDiffusivity_(const Problem &problem,
                                   const Element &element,
                                   const ElementVolumeVariables &elemVolVars)
        {
            DimVector tmp(0.0);
            moleFraction_ = Scalar(0);
            massFraction_ = Scalar(0);
            densityGrad_ = Scalar(0);

            // calculate gradients and secondary variables at IPs
            for (int idx = 0;
                 idx < this->fvGeometry_.numScv;
                 idx++) // loop over vertices of the element
            {
                moleFraction_ += elemVolVars[idx].fluidState().moleFraction(phaseIdx, transportCompIdx) *
                    this->face().shapeValue[idx];

                massFraction_ += elemVolVars[idx].fluidState().massFraction(phaseIdx, transportCompIdx) *
                    this->face().shapeValue[idx];

                // the density gradient
                tmp = this->face().grad[idx];
                tmp *= elemVolVars[idx].density();
                densityGrad_ += tmp;
            }

            // richardson number (schlichting p472, lehfeldt p82)
            Scalar gravity = 0.0;
            if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
                gravity = problem.gravity()[wallNormal_];
            else
                gravity = 9.81;
            richardsonNumberComponent_ = - gravity / this->density() * densityGrad()[wallNormal_] / (velGradWall_ * velGradWall_);
            // schmidt number
            schmidtNumber_ = this->kinematicViscosity() / this->diffusionCoeff(transportCompIdx); // Sc = \nu / D_AB

            // 0 = no eddy diffusivity
            // D_t = 0.0
            //
            if (eddyDiffusivityModel_ == 0)
            {
                eddyDiffusivity_ = 0.0;
            }

            // 1 = reynolds analogy
            // D_t = \nu_t / Sc_t = \mu_t / (\rho Sc_t)
            // BSL, Transport Phenomena, p 657
            if (eddyDiffusivityModel_ == 1)
            {
                Scalar turbulentSchmidtNumber = 1.0;
                eddyDiffusivity_ = this->kinematicEddyViscosity() / turbulentSchmidtNumber;
            }

            // 2 = modified prandtl van-driest
            // D_t =
            // BSL, Transport Phenomena, p 164
            if (eddyDiffusivityModel_ == 2)
            {
                // eddy diffusivity can only be calculated correctly for non-zero distance to walls
                if (this->distanceToWallReal() > 0)
                {
                    mixingLengthDiffusivity_ =
                        this->karmanConstant() * this->distanceToWallReal()
                        * (1 - std::exp(- this->yPlusReal() / 26.0))
                        / sqrt(1 - std::exp(-.26 * this->yPlusReal()));
                }
                eddyDiffusivity_ = mixingLengthDiffusivity_ * mixingLengthDiffusivity_ * fabs(velGrad_);
            }

            // 3 = meier rotta
            // D_t =
            // Cebeci, Analysis of turbulent boundary layer, p 251f
            if (eddyDiffusivityModel_ == 3)
            {
                // Sc_t at flow = 0.86
                // Sc_t in wall = 1.34
                Scalar kappaMeier = this->karmanConstant() / sqrt(0.86);
                Scalar aPlusMeier = sqrt(1.34) / sqrt(0.86) * 26;
                mixingLengthDiffusivity_ =
                    kappaMeier * this->distanceToWallReal()
                    * (1 - std::exp(- this->yPlusReal() / aPlusMeier));
                eddyDiffusivity_  = mixingLengthDiffusivity_ * mixingLengthDiffusivity_ * fabs(velGrad_);
            }

            // 4 = rohsenow cohen
            // D_t = 416 * Sc * (1/15 * 6/pi^4 * SUM)
            // rohsenow and choi p 191
            if (eddyDiffusivityModel_ == 4)
            {
                Scalar sum = 0.0;
                Scalar sumTemp = 1.0;
                Scalar sumEps = 1e-11;
                for (int sumIdx = 1; (sumIdx < 50 && sumTemp > sumEps); ++sumIdx)
                {
                    sumTemp = std::exp(-.0024 * M_PI * M_PI * sumIdx * sumIdx / schmidtNumber_) / std::pow(sumIdx, 4.0);
                    sum += sumTemp;
                }
                eddyDiffusivity_ = 416.0 * schmidtNumber_ * (1.0 / 15.0 - 6.0 / std::pow(M_PI, 4.0) * sum) * this->kinematicEddyViscosity();
            }

            // 5 = exponential (nach Mamayev)
            // D_t = exp(- m * Ri) * \nu_t
            // Lehfeldt, Ein algebraisches Turbulenzmodell für Ästuare, p 65
            if (eddyDiffusivityModel_ == 5)
            {
                Scalar m = GET_RUNTIME_PARAM(TypeTag, Scalar, ZeroEq.MExponentialDiffusivity);
                if (velGradWall_ == 0) // means incredible high Ri
                    eddyDiffusivity_ = 0.0;
                else
                    eddyDiffusivity_ = std::exp(- m * richardsonNumberComponent_) * this->kinematicEddyViscosity();
            }

            // 6 = henderson sellers
            // D_t = \nu_t / (1 + 37 * Ri * Ri)
            // Lehfeldt, Ein algebraisches Turbulenzmodell für Ästuare, p 65f
            if (eddyDiffusivityModel_ == 6)
            {
                if (velGradWall_ == 0) // means incredible high Ri
                    eddyDiffusivity_ = 0.0;
                else if (richardsonNumberComponent_ == 0)
                    eddyDiffusivity_ = 1.0 * this->kinematicEddyViscosity(); // Lehfeldt p63
                else
                    eddyDiffusivity_ = 1.0 / (1 + 37 * richardsonNumberComponent_ * richardsonNumberComponent_)
                                       * this->kinematicEddyViscosity();
            }

            // 7 = deissler near wall law
            // \mu_t = \beta ( 1 - std::exp( - \beta / \mu ) )
            // \beta = \rho * n^2 * v_x * y
            // n = 0.124 (deissler law constant)
            // D_t = \mu_t / \varrho
            if (eddyDiffusivityModel_ == 7)
            {
                const Scalar deisslerConstant = 0.124;
                const Scalar beta = this->density() * deisslerConstant * deisslerConstant
                                    * std::abs(this->velocity()[flowNormal_])
                                    * this->distanceToWallReal();
                eddyDiffusivity_ = beta * (1.0 - std::exp( - 1.0 * beta / this->dynamicViscosity()));
                eddyDiffusivity_ /= this->density();
            }

            Valgrind::CheckDefined(moleFraction_);
            Valgrind::CheckDefined(massFraction_);
            Valgrind::CheckDefined(densityGrad_);
        }


public:

    /*!
     * \brief Return the mole fraction \f$\mathrm{[-]}\f$ of the transported component
     *        at the integration point.
     */
    const Scalar moleFraction() const
    { return moleFraction_; }

    /*!
     * \brief Return the mass fraction \f$\mathrm{[-]}\f$ of the transported component
     *        at the integration point.
     */
    const Scalar massFraction() const
    { return massFraction_; }

    /*!
     * \brief Return the density Gradient \f$\mathrm{[kg/m^4]}\f$.
     */
    const DimVector &densityGrad() const
    { return densityGrad_; }

    /*!
     * \brief Return the eddy diffusivity \f$\mathrm{[m^2/s]}\f$.
     */
    const Scalar eddyDiffusivity() const
    { return eddyDiffusivity_; }

    /*!
     * \brief Return the eddy diffusion model.
     */
    const Scalar eddyDiffusivityModel() const
    { return eddyDiffusivityModel_; }

    /*!
     * \brief Return schmidt number (molecular) \f$\mathrm{[-]}\f$.
     */
    const Scalar schmidtNumber() const
    { return schmidtNumber_; }

    /*!
     * \brief Return richardson number \f$\mathrm{[-]}\f$.
     */
    const Scalar richardsonNumberComponent() const
    { return richardsonNumberComponent_; }

    /*!
     * \brief The name of the diffusivity Model.
     */
    const char *eddyDiffusivityModelName() const
    {
        if (eddyDiffusivityModel_ == 1)
            return "reynolds";
        else if (eddyDiffusivityModel_ == 2)
            return "modVanDriest";
        else if (eddyDiffusivityModel_ == 3)
            return "meier";
        else if (eddyDiffusivityModel_ == 4)
            return "rohensow";
        else if (eddyDiffusivityModel_ == 5)
            return "exponential";
        else if (eddyDiffusivityModel_ == 6)
            return "henderson";
        else if (eddyDiffusivityModel_ == 7)
            return "deissler";
        else return "noEddyDiffMod";
    }

private:
        const int flowNormal_;
        const int wallNormal_;
        const int eddyDiffusivityModel_;

        Scalar velGrad_;
        Scalar velGradWall_;
        int posIdx_;
        int wallIdx_;
        DimVector globalPos_;

        Scalar eddyDiffusivity_;
        Scalar mixingLengthDiffusivity_;
        DimVector densityGrad_;
        Scalar moleFraction_;
        Scalar massFraction_;

        Scalar richardsonNumberComponent_;
        Scalar schmidtNumber_;
};

} // end namespace

#endif // DUMUX_ZEROEQ2C_FLUX_VARIABLES_HH
