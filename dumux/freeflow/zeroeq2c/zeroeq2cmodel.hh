// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Thomas Fetzer                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Adaptation of the box scheme to the compositional ZeroEq model.
 *
 */
#ifndef DUMUX_ZEROEQ2C_MODEL_HH
#define DUMUX_ZEROEQ2C_MODEL_HH

#include <dumux/freeflow/stokesnc/stokesncmodel.hh>
#include <dumux/freeflow/zeroeq/zeroeqmodel.hh>

#include "zeroeq2cproperties.hh"

namespace Dumux {
/*!
 * \ingroup BoxStokesncModel
 * \ingroup BoxZeroEq2cModel
 * \brief Adaptation of the BOX scheme to the compositional ZeroEq model.
 *
 * This model implements an compositional ZeroEq Navier Stokes flow of a fluid
 * solving a momentum balance, a mass balance and a conservation equation for one component.
 *
 * \todo update balance equations
 * Momentum Balance (has to be updated):
 * \f[
 *   \frac{\partial \left(\varrho_g {\boldsymbol{v}}_g\right)}{\partial t}
 *   + \boldsymbol{\nabla} \boldsymbol{\cdot} \left(p_g {\bf {I}}
 *   - \mu_g \left(\boldsymbol{\nabla} \boldsymbol{v}_g
 *   + \boldsymbol{\nabla} \boldsymbol{v}_g^T\right)\right)
 *   - \varrho_g {\bf g} = 0,
 * \f]
 *
 * Mass balance (has to be updated):
 * \f[
 *  \frac{\partial \varrho_g}{\partial t} + \boldsymbol{\nabla}\boldsymbol{\cdot}\left(\varrho_g {\boldsymbol{v}}_g\right) - q_g = 0
 * \f]
 *
 * Component mass balance equation (has to be updated):
 * \f[
 *  \frac{\partial \left(\varrho_g X_g^\kappa\right)}{\partial t}
 *   + \boldsymbol{\nabla} \boldsymbol{\cdot} \left( \varrho_g {\boldsymbol{v}}_g X_g^\kappa
 *   - D^\kappa_g \varrho_g \boldsymbol{\nabla} X_g^\kappa \right)
 *   - q_g^\kappa = 0
 * \f]
 *
 * This is discretized using a fully-coupled vertex
 * centered finite volume (box) scheme as spatial and
 * the implicit Euler method as temporal discretization.
 */
template<class TypeTag>
class ZeroEq2cModel : public ZeroEqModel<TypeTag>
{
    typedef ZeroEqModel<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        intervals = GET_PROP_VALUE(TypeTag, NumberOfIntervals),
        walls = GET_PROP_VALUE(TypeTag, NumberOfWalls),
        prec = 4, // precision of scv data
        width = prec + 7 // width of column
    };
    enum { numEq = GET_PROP_VALUE(TypeTag, NumEq) };
    enum { transportCompIdx = Indices::transportCompIdx };
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef Dune::ReferenceElements<Scalar, dim> ReferenceElements;
    typedef Dune::ReferenceElement<Scalar, dim> ReferenceElement;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> DimVector;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;


public:
    ZeroEq2cModel()
        :   flowNormal_(GET_PARAM(TypeTag, int, FlowNormal))
        ,   wallNormal_(GET_PARAM(TypeTag, int, WallNormal))
    {
        eps_ = 1e-6;
    }

    //! \copydoc BoxModel::addOutputVtkFields
    template <class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, dim> > VelocityField;

        const Scalar scale_ = GET_PROP_VALUE(TypeTag, Scaling);

        // create the required scalar fields
        unsigned numVertices = this->gridView_().size(dim);
        ScalarField &pN = *writer.allocateManagedBuffer(numVertices);
        ScalarField &delP = *writer.allocateManagedBuffer(numVertices);
        ScalarField &Xw = *writer.allocateManagedBuffer(numVertices);
        ScalarField &rho = *writer.allocateManagedBuffer(numVertices);
        ScalarField &mu = *writer.allocateManagedBuffer(numVertices);
        VelocityField &velocity = *writer.template allocateManagedBuffer<Scalar, dim> (numVertices);

        unsigned numElements = this->gridView_().size(0);
        ScalarField &rank = *writer.allocateManagedBuffer(numElements);

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;
        ElementBoundaryTypes elemBcTypes;

        ElementIterator elemIt = this->gridView_().template begin<0>();
        ElementIterator endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            int idx = this->elementMapper().map(*elemIt);
            rank[idx] = this->gridView_().comm().rank();

            fvGeometry.update(this->gridView_(), *elemIt);
            elemBcTypes.update(this->problem_(), *elemIt, fvGeometry);

            int numLocalVerts = elemIt->template count<dim>();
            for (int i = 0; i < numLocalVerts; ++i)
            {
                int globalIdx = this->vertexMapper().map(*elemIt, i, dim);
                volVars.update(sol[globalIdx],
                               this->problem_(),
                               *elemIt,
                               fvGeometry,
                               i,
                               false);

                pN[globalIdx] = volVars.pressure()*scale_;
                delP[globalIdx] = volVars.pressure()*scale_ - 1e5;
                Xw[globalIdx] = volVars.fluidState().massFraction(phaseIdx, transportCompIdx);
                rho[globalIdx] = volVars.density()*scale_*scale_*scale_;
                mu[globalIdx] = volVars.dynamicViscosity()*scale_;
                velocity[globalIdx] = volVars.velocity();
                velocity[globalIdx] *= 1/scale_;
            }
        }
        writer.attachVertexData(pN, "P");
        writer.attachVertexData(delP, "delP");
        std::ostringstream outputNameX;
        outputNameX << "X^" << FluidSystem::componentName(transportCompIdx);
        writer.attachVertexData(Xw, outputNameX.str());
        writer.attachVertexData(rho, "rho");
        writer.attachVertexData(mu, "mu");
        writer.attachVertexData(velocity, "v", dim);


        // just to have the actual values plotted
        asImp_().updateWallProperties(sol);

        elemIt = this->gridView_().template begin<0>();
        endit = this->gridView_().template end<0>();

        for (; elemIt != endit; ++elemIt)
        {
            fvGeometry.update(this->gridView_(), *elemIt);
            elemBcTypes.update(this->problem_(), *elemIt, fvGeometry);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(this->problem_(),
                               *elemIt,
                               fvGeometry,
                               false);

            IntersectionIterator isIt = this->gridView_().ibegin(*elemIt);
            const IntersectionIterator &endIt = this->gridView_().iend(*elemIt);

            for (; isIt != endIt; ++isIt)
            {
                int faceIdx = isIt->indexInInside();

                FluxVariables fluxVars(this->problem_(),
                                                    *elemIt,
                                                    fvGeometry,
                                                    faceIdx,
                                                    elemVolVars,
                                                    false);

                GlobalPosition globalPos = fvGeometry.subContVolFace[faceIdx].ipGlobal;

                if (asImp_().shouldWriteSCVData(globalPos))
                asImp_().writeSCVData(volVars, fluxVars, globalPos);
            }
        }
    }

    //! \copydoc ZeroEqModel::shouldWriteSCVData
    const bool shouldWriteSCVData (const GlobalPosition &global) const
    {
        return ParentType::shouldWriteSCVData(global);
    }

    //! \copydoc ZeroEqModel::writeSCVHeader
    void writeSCVHeader(std::stringstream &stream, const FluxVariables &fluxVars, const GlobalPosition &global)
    {
        ParentType::writeSCVHeader(stream, fluxVars, global);
        stream << std::setw(width) << "eddyDMod" << std::setw(width) << GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyDiffusivityModel)
                << std::setw(2*width) << fluxVars.eddyDiffusivityModelName();
    }

    //! \copydoc ZeroEqModel::writeWallHeader
    void writeWallHeader(std::stringstream &stream, int posIdx, int wallIdx)
    {
        ParentType::writeWallHeader(stream, posIdx, wallIdx);
        stream << std::setw(width) << "maxMassFr" << std::setw(width) << std::setprecision(prec) << this->wall[wallIdx].maxMassFraction[posIdx]
                        << std::setw(width) << "maxMoleFr" << std::setw(width) << std::setprecision(prec) << this->wall[wallIdx].maxMoleFraction[posIdx];
    }

    //! \copydoc ZeroEqModel::writeDataHeader
    void writeDataHeader(std::stringstream &stream, int posIdx)
    {
        ParentType::writeDataHeader(stream, posIdx);
        stream << std::setw(width) << "X"
               << std::setw(width) << "X^/X^_max"
               << std::setw(width) << "N"
               << std::setw(width) << "N^/N^_max"
               << std::setw(width) << "eddyDiff"
               << std::setw(width) << "densGrad"
               << std::setw(width) << "Sc"
               << std::setw(width) << "Sc_t"
               << std::setw(width) << "D_t/D"
               << std::setw(width) << "RiC";
    }

    //! \copydoc ZeroEqModel::writeSCVDataValues
    void writeSCVDataValues(std::stringstream &stream, const VolumeVariables &volVars, const FluxVariables &fluxVars, const GlobalPosition &global)
    {
        ParentType::writeSCVDataValues(stream, volVars, fluxVars, global);

        int posIdx = this->getPosIdx(global);
        int wallIdx = this->getWallIdx(global, posIdx);
        stream << std::setw(width) << std::setprecision(prec) << fluxVars.massFraction()
                << std::setw(width) << std::setprecision(prec) << fluxVars.massFraction() / this->wall[wallIdx].maxMassFraction[posIdx]
                << std::setw(width) << std::setprecision(prec) << fluxVars.moleFraction()
                << std::setw(width) << std::setprecision(prec) << fluxVars.moleFraction() / this->wall[wallIdx].maxMoleFraction[posIdx]
                << std::setw(width) << std::setprecision(prec) << fluxVars.eddyDiffusivity()
                << std::setw(width) << std::setprecision(prec) << fluxVars.densityGrad()[wallNormal_]
                << std::setw(width) << std::setprecision(prec) << fluxVars.schmidtNumber()
                << std::setw(width) << std::setprecision(prec) << fluxVars.kinematicEddyViscosity() / fluxVars.eddyDiffusivity()
                << std::setw(width) << std::setprecision(prec) << fluxVars.eddyDiffusivity() / fluxVars.diffusionCoeff(transportCompIdx)
                << std::setw(width) << std::setprecision(prec) << fluxVars.richardsonNumberComponent();
    }


    //! \copydoc ZeroEqModel::resetWallProperties
    void resetWallProperties()
    {
        ParentType::resetWallProperties();

        for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
            for (int posIdx = 0; posIdx < intervals; ++posIdx)
            {
                this->wall[wallIdx].maxMassFraction[posIdx] = 0.0;
                this->wall[wallIdx].maxMoleFraction[posIdx] = 0.0;
            }
    }

    //! \copydoc ZeroEqModel::calculateMaxFluxVars
    void calculateMaxFluxVars(const FVElementGeometry &fvGeometry, const FluxVariables &fluxVars, const GlobalPosition &globalPos)
    {
        ParentType::calculateMaxFluxVars(fvGeometry, fluxVars, globalPos);

        int posIdx = this->getPosIdx(globalPos);
        int wallIdx = this->getWallIdx(globalPos, posIdx);
        // mass fraction
        if (this->wall[wallIdx].maxMassFraction[posIdx] < fluxVars.massFraction())
            for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
                this->wall[wallIdx].maxMassFraction[posIdx] = fluxVars.massFraction();

        // mole fraction
        if (this->wall[wallIdx].maxMoleFraction[posIdx] < fluxVars.moleFraction())
            for (int wallIdx = 0; wallIdx < walls; ++wallIdx)
                this->wall[wallIdx].maxMoleFraction[posIdx] = fluxVars.moleFraction();
    }

    //! \copydoc ZeroEqModel::doInterpolationFluxValues
    const void doInterpolationFluxValues (const int wallIdx, const int posIdx, const int prevIdx, const int nextIdx)
    {
        ParentType::doInterpolationFluxValues(wallIdx, posIdx, prevIdx, nextIdx);
        this->wall[wallIdx].maxMassFraction[posIdx] = this->interpolation(posIdx, prevIdx, this->wall[wallIdx].maxMassFraction[prevIdx], nextIdx, this->wall[wallIdx].maxMassFraction[nextIdx]);
        this->wall[wallIdx].maxMoleFraction[posIdx] = this->interpolation(posIdx, prevIdx, this->wall[wallIdx].maxMoleFraction[prevIdx], nextIdx, this->wall[wallIdx].maxMoleFraction[nextIdx]);
    }

    //! \copydoc ZeroEqModel::writeWallPropertiesHeader
    void writeWallPropertiesHeader(std::stringstream &stream)
    {
        ParentType::writeWallPropertiesHeader(stream);
        stream << std::setw(width) << "maxMassF"
                << std::setw(width) << "maxMoleF";
    }

    //! \copydoc ZeroEqModel::writeWallPropertiesValues
    void writeWallPropertiesValues(std::stringstream &stream, int wallIdx, int posIdx)
    {
        ParentType::writeWallPropertiesValues(stream, wallIdx, posIdx);
        stream << std::setw(width) << std::setprecision(prec) << this->wall[wallIdx].maxMassFraction[posIdx]
                << std::setw(width) << std::setprecision(prec) << this->wall[wallIdx].maxMoleFraction[posIdx];
    }

private:
    Scalar eps_;
    const int flowNormal_;
    const int wallNormal_;

    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};

}

#include "zeroeq2cpropertydefaults.hh"

#endif // DUMUX_ZEROEQ2C_MODEL_HH
