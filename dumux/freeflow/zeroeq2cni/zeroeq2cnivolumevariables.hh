// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the supplemental quantities, which are constant within a
 *        finite volume in the non-isothermal compositional ZeroEq box model.
 */
#ifndef DUMUX_ZEROEQ2CNI_VOLUME_VARIABLES_HH
#define DUMUX_ZEROEQ2CNI_VOLUME_VARIABLES_HH

#include <dumux/freeflow/stokesncni/stokesncnivolumevariables.hh>
#include "zeroeq2cniproperties.hh"

namespace Dumux
{

/*!
 * \ingroup BoxStokesncniModel
 * \ingroup BoxZeroEq2cniModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the non-isothermal compositional ZeroEq
 *        box model.
 */
template <class TypeTag>
class ZeroEq2cniVolumeVariables : public StokesncniVolumeVariables<TypeTag>
{
    typedef StokesncniVolumeVariables<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };
    enum { temperatureIdx = Indices::temperatureIdx };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

public:
    /*!
     * \copydoc BoxVolumeVariables::update()
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx,
                bool isOldSol)
    {
        // vertex update data for the mass balance
        ParentType::update(priVars,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);

        heatCapacity_ = FluidSystem::heatCapacity(this->fluidState(), phaseIdx);

        thermalConductivity_ = FluidSystem::thermalConductivity(this->fluidState(), phaseIdx);
    };


    /*!
     * \brief Return the heat capacity \f$\mathrm{[J / (kg * K)]}\f$.
     */
    Scalar heatCapacity() const
    { return heatCapacity_; };

    /*!
     * \brief Return the thermal conductivity \f$\mathrm{[W / (m * K)]}\f$.
     */
    Scalar thermalConductivity() const
    { return thermalConductivity_; };


protected:
    Scalar heatCapacity_;
    Scalar thermalConductivity_;
};

} // end namespace

#endif
