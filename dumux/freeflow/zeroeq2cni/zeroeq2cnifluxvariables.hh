// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Thomas Fetzer                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief This file contains the data which is required to calculate
 *        the fluxes of the
 *        non-isothermal compositional ZeroEq model
 *        over a face of a finite volume.
 *
 * This means pressure gradients, phase densities at the integration point, etc
 */
#ifndef DUMUX_ZEROEQ2CNI_FLUX_VARIABLES_HH
#define DUMUX_ZEROEQ2CNI_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/common/valgrind.hh>

#include <dumux/freeflow/zeroeq2c/zeroeq2cfluxvariables.hh>
#include <dumux/freeflow/zeroeq2cni/zeroeq2cniproperties.hh>

namespace Dumux
{

/*!
 * \ingroup BoxStokesncniModel
 * \ingroup BoxZeroEq2cniModel
 * \brief This template class contains the data which is required to
 *        calculate the mass and momentum fluxes over the face of a
 *        sub-control volume for the
 *        non-isothermal compositional ZeroEq model.
 *
 * This means pressure gradients, phase densities, viscosities, etc.
 * at the integration point of the sub-control-volume face
 */
template <class TypeTag>
class ZeroEq2cniFluxVariables : public ZeroEq2cFluxVariables<TypeTag>
{
    typedef ZeroEq2cFluxVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension,
        phaseIdx = Indices::phaseIdx,
        transportCompIdx = Indices::transportCompIdx
        };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

public:
    ZeroEq2cniFluxVariables(const Problem &problem,
                        const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int faceIdx,
                        const ElementVolumeVariables &elemVolVars,
                        const bool onBoundary = false)
        : ParentType(problem, element, fvGeometry, faceIdx, elemVolVars, onBoundary)
        , flowNormal_(GET_PARAM(TypeTag, int, FlowNormal))
        , wallNormal_(GET_PARAM(TypeTag, int, WallNormal))
        , eddyConductivityModel_(GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyConductivityModel))
    {
        globalPos_ = this->globalPos();
        posIdx_ = problem.model().getPosIdx(globalPos_);
        wallIdx_ = problem.model().getWallIdx(globalPos_, posIdx_);
        velGrad_ = this->velocityGrad_[flowNormal_][wallNormal_];
        velGradWall_ = problem.model().wall[wallIdx_].wallVelGrad[posIdx_];
        temperatureEddyConductivity_ = Scalar(0);
        mixingLengthConductivity_ = Scalar(0);
        richardsonNumberEnergy_ = this->richardsonNumberComponent(); // Lehfeldt proposes only one richardson number for component and energy
        prandtlNumber_ = 0.0;

        if (GET_PARAM(TypeTag, bool, EnableNavierStokes)) // calculation of an eddy conductivity only makes sense WITH navier stokes term
            calculateEddyConductivity_(problem, element, elemVolVars);
    };

protected:
    /*!
    * \brief This functions calculates a thermalEddyConductivity (which is added
    *        to the thermalConductivity in stokesncnilocalresidual.hh) at each scv
    *        face.
    */
    void calculateEddyConductivity_(const Problem &problem,
                                          const Element &element,
                                          const ElementVolumeVariables &elemVolVars)
        {
            temperature_ = 0.0;
            heatCapacity_ = 0.0;
            thermalConductivity_ = 0.0;
            for (int idx = 0;
                 idx < this->fvGeometry_.numScv;
                 idx++) // loop over adjacent vertices
            {
                // phase temperature, heat capacity and thermal conductivity at IP
                temperature_ += elemVolVars[idx].temperature() *
                    this->face().shapeValue[idx];
                heatCapacity_ += elemVolVars[idx].heatCapacity() *
                    this->face().shapeValue[idx];
                thermalConductivity_ += elemVolVars[idx].thermalConductivity() *
                    this->face().shapeValue[idx];
            }

            prandtlNumber_ = this->dynamicViscosity() * heatCapacity() / thermalConductivity(); // Pr = \mu * c_p / \lambda

            // the temperatureEddyConductivity_ a_t [m^2/s] is converted to
            // thermalEddyConductivity \lambda_t [W/(m K)] by the convenience function

            // 0 = no eddy conductivity
            // a_t = D_t =0.0
            //
            if (eddyConductivityModel_ == 0)
            {
                temperatureEddyConductivity_ = 0.0;
            }

            // 1 = reynolds analogy
            // a_t = D_t = \nu_t / Pr_t = \mu_t / (\rho * Pr_t)
            // BSL, Transport Phenomena, p 657
            if (eddyConductivityModel_ == 1)
            {
                Scalar turbulentPrandtlNumber = 1.0;
                temperatureEddyConductivity_ = this->kinematicEddyViscosity() / turbulentPrandtlNumber;
            }

            // 2 = modified prandtl van-driest
            // a_t = D_t =
            // BSL, Transport Phenomena, p 164
            if (eddyConductivityModel_ == 2)
            {
                // eddy conductivity can only be calculated correctly for non-zero distance to walls
                if (this->distanceToWallReal() > 0)
                {
                    mixingLengthConductivity_ =
                            this->karmanConstant() * this->distanceToWallReal()
                            * (1 - std::exp(- this->yPlusReal() / 26.0))
                            / sqrt(1 - std::exp(-.26 * this->yPlusReal()));
                }
                temperatureEddyConductivity_ = mixingLengthConductivity_ * mixingLengthConductivity_ * fabs(velGrad_);
            }

            // 3 = meier rotta
            // a_t = D_t =
            // Cebeci, Analysis of turbulent boundary layer, p 251f
            if (eddyConductivityModel_ == 3)
            {
                // Sc_t at flow = 0.86
                // Sc_t in wall = 1.34
                Scalar kappaMeier = this->karmanConstant() / sqrt(0.86);
                Scalar aPlusMeier = sqrt(1.34) / sqrt(0.86) * 26;
                mixingLengthConductivity_ =
                    kappaMeier * this->distanceToWallReal()
                    * (1 - std::exp(- this->yPlusReal() / aPlusMeier));
                temperatureEddyConductivity_  = mixingLengthConductivity_ * mixingLengthConductivity_ * fabs(velGrad_);
            }

            // 4 = rohensow Cohen
            // a_t = D_t = 416 * Sc * (1/15 * 6/pi^4 * SUM)
            // Cebeci, Analysis of turbulent boundary layer, p 241f
            if (eddyConductivityModel_ == 4)
            {
                Scalar sum = 0.0;
                Scalar sumTemp = 1.0;
                Scalar sumEps = 1e-11;
                for (int sumIdx = 1; (sumIdx < 50 && sumTemp > sumEps); ++sumIdx)
                {
                    sumTemp = std::exp(-.0024 * M_PI * M_PI * sumIdx * sumIdx / prandtlNumber()) / std::pow(sumIdx, 6.0);
                    sum += sumTemp;
                }
                temperatureEddyConductivity_ = 416.0 * prandtlNumber() * (1.0 / 15.0 - 6.0 / std::pow(M_PI, 4.0) * sum)
                                               * this->kinematicEddyViscosity();
            }

            // 5 = exponential (nach Mamayev)
            // a_t = D_t = exp(- m * Ri) * \nu_t
            // Lehfeldt, Ein algebraisches Turbulenzmodell für Ästuare, p 65
            if (eddyConductivityModel_ == 5)
            {
                Scalar m = GET_RUNTIME_PARAM(TypeTag, Scalar, ZeroEq.MExponentialConductivity);
                if (velGradWall_ == 0) // means incredible high Ri
                    temperatureEddyConductivity_ = 0.0;
                else
                    temperatureEddyConductivity_ = std::exp(- m * richardsonNumberEnergy()) * this->kinematicEddyViscosity();
            }

            // 6 = henderson sellers
            // a_t = D_t = \nu_t / (1 + 37 * Ri * Ri)
            // Lehfeldt, Ein algebraisches Turbulenzmodell für Ästuare, p 65f
            if (eddyConductivityModel_ == 6)
            {
                if (velGradWall_ == 0) // means incredible high Ri
                    temperatureEddyConductivity_ = 0.0;
                else if (richardsonNumberEnergy_ == 0)
                    temperatureEddyConductivity_ = 1.0 * this->kinematicEddyViscosity(); // Lehfeldt p63
                else
                    temperatureEddyConductivity_ = 1.0 / (1 + 37 * richardsonNumberEnergy() * richardsonNumberEnergy())
                                                   * this->kinematicEddyViscosity();
            }

            // 7 = deissler near wall law
            // \mu_t = \beta ( 1 - std::exp( - \beta / \mu ) )
            // \beta = \rho * n^2 * v_x * y
            // n = 0.124 (deissler law constant)
            // a_t = \mu_t / \varrho
            if (eddyConductivityModel_ == 7)
            {
                const Scalar deisslerConstant = 0.124;
                const Scalar beta = this->density() * deisslerConstant * deisslerConstant
                                    * std::abs(this->velocity()[flowNormal_])
                                    * this->distanceToWallReal();
                temperatureEddyConductivity_ = beta * (1.0 - std::exp( - 1.0 * beta / this->dynamicViscosity()));
                temperatureEddyConductivity_ /= this->density();
            }
            Valgrind::CheckDefined(temperature_);
            Valgrind::CheckDefined(heatCapacity_);
            Valgrind::CheckDefined(thermalConductivity_);
            Valgrind::CheckDefined(temperatureEddyConductivity_);
        }


public:
    /*!
     * \brief Return the temperature \f$\mathrm{[K]}\f$.
     */
    const Scalar temperature() const
    { return temperature_; }

    /*!
     * \brief Return the heat capacity \f$\mathrm{[J / (kg * K)]}\f$.
     */
    const Scalar heatCapacity() const
    { return heatCapacity_; }

    /*!
     * \brief Return the thermal conductivity \f$\mathrm{[W / (m * K)]}\f$.
     */
    const Scalar thermalConductivity() const
    { return thermalConductivity_; }

    /*!
     * \brief Return the thermal eddy conductivity \f$\mathrm{[W / (m * K)]}\f$.
     */
    const Scalar thermalEddyConductivity() const
    { return temperatureEddyConductivity() * this->density() * heatCapacity();; }

    /*!
     * \brief Return the temperature eddy conductivity \f$\mathrm{[m^2/s]}\f$.
     */
    const Scalar temperatureEddyConductivity() const
    { return temperatureEddyConductivity_; }

    /*!
     * \brief Return the eddy conductivity model.
     */
    const Scalar eddyConductivityModel() const
    { return eddyConductivityModel_; }

    /*!
     * \brief Return prandtl number (molecular) \f$\mathrm{[-]}\f$.
     */
    const Scalar prandtlNumber() const
    { return prandtlNumber_; }

    /*!
     * \brief Return richardson number \f$\mathrm{[-]}\f$.
     */
    const Scalar richardsonNumberEnergy() const
    { return richardsonNumberEnergy_; }

    /*!
     * \brief The name of the eddy conductivity Model.
     */
    const char *eddyConductivityModelName() const
    {
        if (eddyConductivityModel_ == 1)
            return "reynolds";
        else if (eddyConductivityModel_ == 2)
            return "modVanDriest";
        else if (eddyConductivityModel_ == 3)
            return "meier";
        else if (eddyConductivityModel_ == 4)
            return "rohensow";
        else if (eddyConductivityModel_ == 5)
            return "exponential";
        else if (eddyConductivityModel_ == 6)
            return "henderson";
        else if (eddyConductivityModel_ == 7)
            return "deissler";
        else return "noEddyCondMod";
    }

private:
        const int flowNormal_;
        const int wallNormal_;
        const int eddyConductivityModel_;

        Scalar velGrad_;
        Scalar velGradWall_;
        int posIdx_;
        int wallIdx_;
        DimVector globalPos_;

        Scalar temperature_;
        Scalar heatCapacity_;
        Scalar thermalConductivity_;

        Scalar temperatureEddyConductivity_;
        Scalar mixingLengthConductivity_;

        Scalar richardsonNumberEnergy_;
        Scalar prandtlNumber_;
};

} // end namespace

#endif // DUMUX_ZEROEQ2CNI_FLUX_VARIABLES_HH
