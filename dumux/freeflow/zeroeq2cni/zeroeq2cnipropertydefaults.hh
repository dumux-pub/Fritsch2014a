// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Thomas Fetzer                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesncniModel
 * \ingroup BoxZeroEq2cniModel
 * \file
 *
 * \brief Defines default properties for the ZeroEq
 *        non-isothermal compositional ZeroEq box model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 *
 */

#ifndef DUMUX_ZEROEQ2CNI_PROPERTY_DEFAULTS_HH
#define DUMUX_ZEROEQ2CNI_PROPERTY_DEFAULTS_HH

#include <dumux/freeflow/stokesncni/stokesncnifluxvariables.hh>
#include <dumux/freeflow/stokesncni/stokesncnimodel.hh>
#include <dumux/freeflow/stokesncni/stokesncnivolumevariables.hh>
#include "zeroeq2cnifluxvariables.hh"
#include "zeroeq2cnivolumevariables.hh"
#include "zeroeq2cnimodel.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////


// INHERITED
//! Set the Model property
SET_TYPE_PROP(BoxZeroEq2cni, Model, ZeroEq2cniModel<TypeTag>);

//! Set the FluxVariables property
SET_TYPE_PROP(BoxZeroEq2cni, FluxVariables, ZeroEq2cniFluxVariables<TypeTag>);

//! Set the VolumeVariables property
SET_TYPE_PROP(BoxZeroEq2cni, VolumeVariables, ZeroEq2cniVolumeVariables<TypeTag>);

//! Set calculation to Navier-Stokes
SET_BOOL_PROP(BoxZeroEq2cni, EnableNavierStokes, true);

//! Don't use mole fractions
SET_BOOL_PROP(BoxZeroEq2cni, UseMoles, false);

// NEW PROPERTIES
//! Set type of Eddy Conductivity Model
SET_INT_PROP(BoxZeroEq2cni, ZeroEqEddyConductivityModel, 1);

//! Set the BaseStokesModel to StokesncniModel
SET_TYPE_PROP(BoxZeroEq2cni, BaseStokesModel, StokesncniModel<TypeTag>);

//! Set the BaseStokesFluxVariables to StokesncniFluxVariables
SET_TYPE_PROP(BoxZeroEq2cni, BaseStokesFluxVariables, StokesncniFluxVariables<TypeTag>);

//! Set the BaseStokesVolumeVariables to StokesncniVolumeVariables
SET_TYPE_PROP(BoxZeroEq2cni, BaseStokesVolumeVariables, StokesncniVolumeVariables<TypeTag>);
}
}

#endif // DUMUX_ZEROEQ2CNI_PROPERTY_DEFAULTS_HH
