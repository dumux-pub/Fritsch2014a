/*****************************************************************************
 *   Copyright (C) 2007-2009 by Bernd Flemisch, Jochen Fritz                 *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_GSTATRANDOMPERMEABILITY_HH
#define DUMUX_GSTATRANDOMPERMEABILITY_HH

#include <dune/grid/io/file/vtk/common.hh>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <boost/format.hpp>

#include <dune/grid/common/mcmgmapper.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/istl/bvector.hh>

namespace Dumux
{

/*! \brief providing a random permeability field for 1-D, 2-D or 3-D using gstat for gaussian simulation.
 *  \author Jochen Fritz
 *  gstat is an open source software tool which can (among other things) generate
 *  geostatistical random fields. This functionality is used for this class. See www.gstat.org.
 *  To use this class, unpack the zipped gstat tarball in the external directory and build gstat
 *  according to the README file or just copy the binary on the site into the external folder - the init()
 *  functions searches for the installation of gstat. You have to provide a control file for gstat (examples can be found
 *  in the gstat tarball in the subdirectory DUMUX_stuff).
 */
template<class GridView, class Scalar>
class GstatRandomPermeability
{
    template<int dim>
    struct ElementLayout
    {
        bool contains(Dune::GeometryType gt)
        {
            return gt.dim() == dim;
        }
    };

    enum
        {
            dim = GridView::dimension, dimWorld = GridView::dimensionworld
        };
    typedef Dune::BlockVector<Dune::FieldVector<Scalar,1> > PermType;
    typedef Dune::BlockVector<Dune::FieldVector<Scalar,1> > RepresentationType;
    typedef typename GridView::Traits::template Codim<0>::Entity Element;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::IndexSet IndexSet;
    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, ElementLayout> ElementMapper;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*! \brief Constructor.
     *  If a randompermeability field is to be generated, three filenames must be known:
     *  - a control file, where the commands and the input and output files for gstat are specified.
     *  - a gstat input file (this filename must be same as in the control file). This class writes
     *  the coordinates of the cell centers into the gstat input file and gstat computes
     *  realizations of the random variable at those coordinates.
     *  - a gstat output file (this filename must be same as in the control file). gstat writes the
     *  random values to this file.
     *  If there already is a random field in a gstat output file and you want to reuse it, simply
     *  set create to false and specify the filename.
     *  \param grid reference to grid
     *  \param create set true to create a new field.
     *  \param gstatOut name of the gstat output file
     *  \param gstatCon name of control file for gstat
     *  \param gstatIn name of input file for gstat
     */
    GstatRandomPermeability(const GridView& gridView, const bool create = true, const char* gstatOut = "permeab.dat", const char* gstatCon = "gstatControl.txt", const char* gstatIn = "gstatInput.txt")
        : gridView_(gridView), createNew_(create), perm_(gridView.size(0)), permLoc_(0),
          elementMapper_(gridView)
    {
        init(gstatCon, gstatIn, gstatOut, create);
    }

    void init(const char* gstatCon = "gstatControl.txt", const char* gstatIn = "gstatInput.txt", const char* gstatOut = "permeab.dat", const bool create = true)
    {
        typedef typename GridView::template Codim<0>::Iterator ElementIterator;

         ElementIterator eItEnd = gridView_.template end<0>();

         // search gstat program
         char* pwd(getenv("PWD"));
         char startCommand[220];
         strcpy(startCommand, "find ");
         strcat(startCommand, pwd);
         strcat(startCommand, "/");
         char systemCommand[220];

         char gstatDir[221];
         bool foundGstat = false;
         int k = 0;
         while (!foundGstat && k++ < 10)
         {
             strcpy(systemCommand, startCommand);
             strcat(systemCommand, " -type f -name gstat -exec dirname {} \\; >gstatloc.txt");

             int check = system(systemCommand);
             if (check)
             {
                 DUNE_THROW(Dune::IOError,"Load random permeability field");
             }

             std::ifstream gstatLoc("gstatloc.txt");
             gstatLoc.seekg (0, std::ios::end);
             int length = gstatLoc.tellg();
             gstatLoc.seekg (0, std::ios::beg);
             if (length> 0)
             {
                 foundGstat = true;
                 gstatLoc.getline(gstatDir, 220);
             }
             gstatLoc.close();
             strcat(startCommand, "../");
         }

         if (createNew_)
         {
             // open output stream for simset input file
             char outfileName[100];
             strcpy(outfileName, gstatIn);
             std::ofstream outfile(outfileName);
             for (ElementIterator eIt = gridView_.template begin<0>(); eIt != eItEnd; ++eIt)
             {
                 // get global coordinate of cell center
                 const GlobalPosition& globalPos = eIt->geometry().center();

                 for (int d = 0; d < dim; d++) outfile << globalPos[d] << " "<< std::flush;
                 outfile << std::endl;
             }
             outfile.close();

             std::string syscom;
             syscom = gstatDir;
             syscom += "/gstat ";
             syscom += gstatCon;
             int check = system(syscom.c_str());
             if (check)
             {
                 DUNE_THROW(Dune::IOError,"Load random permeability field");
             }
             syscom = (boost::format("cat %s | python %s/../DUMUX_stuff/stripcrap.py %s %d > mothersLittleHelper.txt; mv mothersLittleHelper.txt %s")
                 %gstatOut%gstatDir%gstatIn%int(dim)%gstatOut).str();
             check = system(syscom.c_str());
             if (check)
             {
                 DUNE_THROW(Dune::IOError,"Load random permeability field");
             }
         }

         char concd[100];
         strcpy(concd, gstatOut);
         std::ifstream infile(gstatOut);
         if (infile.good())
         {
             std::cout << "Read permeability data from " << concd << std::endl;
         }
         else
         {
             DUNE_THROW(Dune::IOError, "file not found: "<<gstatOut );
         }

         std::string zeile;
         std::getline(infile, zeile);

         for (ElementIterator eIt = gridView_.template begin<0>(); eIt != eItEnd; ++eIt)
         {
             int globalIdxI = elementMapper_.map(*eIt);
             Scalar dummy1, dummy2, dummy3, permi;
             std::getline(infile, zeile);
             std::istringstream ist(zeile);
             if (dim == 1)
                 ist >> dummy1 >> permi;
             else if (dim == 2)
                 ist >> dummy1 >> dummy2 >> permi;
             else if (dim == 3)
                 ist >> dummy1 >> dummy2 >> dummy3 >> permi;
             else
                 DUNE_THROW(Dune::NotImplemented, "randompermeability is not implemented for dimension > 3");
             perm_[globalIdxI] = pow(10.0, permi);
         }

         infile.close();
    }

    //! return const reference to permeability vector
    const RepresentationType& operator* () const
    {
        return (perm_);
    }

    //! return reference to permeability vector
    RepresentationType& operator* ()
    {
        return (perm_);
    }

    Dune::FieldMatrix<Scalar,dim,dim>& K (const Element& e)
    {
        int elemId = elementMapper_.map(e);
        Scalar permE = perm_[elemId];

        for (int i = 0; i < dim; i++)
            permLoc_[i][i] = permE;

        return permLoc_;
    }

    void vtkout (const char* name) const
    {
        Dune::VTKWriter<GridView>
            vtkwriter(gridView_);
        vtkwriter.addCellData(perm_, "absolute permeability");
        int size = perm_.size();
        RepresentationType logPerm(size);
        for (int i = 0; i < size; i++)
            logPerm[i] = log10(perm_[i]);
        vtkwriter.addCellData(logPerm, "logarithm of permeability");
        vtkwriter.write(name, Dune::VTK::OutputType::ascii);
    }

private:
    const GridView& gridView_;
    const bool createNew_;
    PermType perm_;
    Dune::FieldMatrix<Scalar,dim,dim> permLoc_;
    ElementMapper elementMapper_;
};

}

#endif

