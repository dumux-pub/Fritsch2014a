// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \ingroup Fluidsystems
 * \brief This extends the h2oairfluidsystem by functions for thermalConductivity
 *        and heatCapacity.
 */
#ifndef DUMUX_H2O_AIRNI_SYSTEM_HH
#define DUMUX_H2O_AIRNI_SYSTEM_HH

#include <cassert>

#include <dumux/material/fluidsystems/h2oairfluidsystem.hh>

#ifdef DUMUX_PROPERTIES_HH
#include <dumux/common/propertysystem.hh>
#include <dumux/common/basicproperties.hh>
#endif

namespace Dumux
{
namespace FluidSystems
{

/*!
 * \ingroup Fluidsystems
 *
 * \brief This extends the h2oairfluidsystem by functions for thermalConductivity
 *        and heatCapacity.
 */
template <class Scalar,
          class H2Otype = Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar> >,
          bool useComplexRelations = true>
class H2OAirNI
: public H2OAir<Scalar, H2Otype, true>
{
public:

    typedef H2OAirNI<Scalar, H2Otype, useComplexRelations > ThisType;
    typedef H2OAir<Scalar, H2Otype, useComplexRelations > Base;

    /*!
     * \brief Thermal conductivity of a fluid phase \f$\mathrm{[W/(m K)]}\f$.
     *
     * Use the conductivity of air and water as a first approximation.
     * Source: http://en.wikipedia.org/wiki/List_of_thermal_conductivities
     *
     * \param fluidState The fluid state
     * \param phaseIdx The phase index
     */
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      int phaseIdx)
    {
        Scalar heatConductivityH2O = 0.0248; // approx at 20 ° C -> http://de.wikipedia.org/wiki/W%C3%A4rmeleitf%C3%A4higkeit [W / (m * K)]
        Scalar heatConductivityAir = 0.0262; // approx at 20 ° C -> http://de.wikipedia.org/wiki/W%C3%A4rmeleitf%C3%A4higkeit [W / (m * K)]
        if (phaseIdx == Base::wPhaseIdx)
        {
            // TODO: influence of air is neglected
            return heatConductivityH2O;
        }
        else if (phaseIdx == Base::nPhaseIdx)
        {
            // TODO: right way to deal with solutes
            return heatConductivityH2O * fluidState.massFraction(Base::nPhaseIdx, Base::wCompIdx)
                   + heatConductivityAir * fluidState.massFraction(Base::nPhaseIdx, Base::nCompIdx);
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/kg/K]}\f$.
     *
     * Use the conductivity of air and water as a first approximation.
     * Source: http://en.wikipedia.org/
     *
     * \param fluidState The fluid state
     * \param phaseIdx The phase index
     */
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        Scalar heatCapacityH2O = 4.182e3; // approx at 20 ° C -> wikipedia.de [J / (kg * K)]
        Scalar heatCapacityAir = 1.005e3; // approx 20 ° C -> wikipedia.de [J / (kg * K)]
        if (phaseIdx == Base::wPhaseIdx)
        {
            // TODO: influence of air is neglected
            return heatCapacityH2O;
        }
        else if (phaseIdx == Base::nPhaseIdx)
        {
            // TODO: right way to deal with solutes
            return heatCapacityH2O * fluidState.massFraction(Base::nPhaseIdx, Base::wCompIdx)
                   + heatCapacityAir * fluidState.massFraction(Base::nPhaseIdx, Base::nCompIdx);
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }
};

} // end namespace FluidSystems

#ifdef DUMUX_PROPERTIES_HH
// forward defintions of the property tags
namespace Properties {
NEW_PROP_TAG(Scalar);
NEW_PROP_TAG(Components);
}

/*!
 * \brief A twophase fluid system with water and air as components.
 *
 * \copydoc DUMUX::H2OAirFluidSystem
 */
template<class TypeTag>
class H2OAirNIFluidSystem
: public FluidSystems::H2OAirNI<typename GET_PROP_TYPE(TypeTag, Scalar),
                                typename GET_PROP(TypeTag, Components)::H2O,
                                GET_PROP_VALUE(TypeTag, EnableComplicatedFluidSystem)>
{};
#endif // DUMUX_PROPERTIES_HH

} // end namespace

#endif // DUMUX_H2O_AIRNI_SYSTEM_HH
