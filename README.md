Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

M. Fritsch<br>
Analysis of Processes and Properties at a Porous-Medium Free-Flow Interface with the Application of Evaporation<br>
Msc Thesis, 2014

You can use the .bib file provided [here](Fritsch2014a.bib).


Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installFritsch2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fritsch2014a/raw/master/installFritsch2014a.sh)
in this folder.

```bash
mkdir -p Fritsch2014a && cd Fritsch2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Fritsch2014a/raw/master/installFritsch2014a.sh
sh ./installFritsch2014a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============

The applications used for this publication can be found in appl/multidomain/interface.
This folder also contains the jobfiles which specify the individual setups.

* __test_2cnizeroeq2p2cni__:
  The basic setup of a wind tunnel coupled to a porous medium below.

In order to run the executable, type:
```bash
cd dumux-Fritsch2014a/appl/multidomain/interface/
make test_2cnizeroeq2p2cni
./test_2cnizeroeq2p2cni
```


Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installFritsch2014a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fritsch2014a/raw/master/installFritsch2014a.sh).

In addition the following external software package are necessary for
compiling the executables:

| software           | version | type          |
| ------------------ | ------- | ------------- |
| automake           | 1.13.4  | build tool    |
| SuperLU            | 4.3     | linear solver |
| ug                 | 3.11.0  | grid manager  |

The module has been checked for the following compilers:

| compiler      | version |
| ------------- | ------- |
| gcc/g++       | 4.7     |
