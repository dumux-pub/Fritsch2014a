#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://github.com/smuething/dune-multidomain.git
git clone https://gitlab.dune-project.org/extensions/dune-multidomaingrid.git
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Fritsch2014a.git dumux-Fritsch2014a

### Go to specific branches
cd dune-common && git checkout releases/2.3 && cd ..
cd dune-geometry && git checkout releases/2.3 && cd ..
cd dune-grid && git checkout releases/2.3 && cd ..
cd dune-istl && git checkout releases/2.3 && cd ..
cd dune-localfunctions && git checkout releases/2.3 && cd ..
cd dune-multidomain && git checkout releases/2.0 && cd ..
cd dune-multidomaingrid && git checkout releases/2.3 && cd ..
cd dune-pdelab && git checkout releases/1.1 && cd ..
cd dumux && git checkout faf24fb07f2ba6187a7a66c0bd99aaaffda7b373 && cd ..
# cd dumux-Fritsch2014a && git checkout master && cd ..

### Go to specific versions
cd dune-common && git checkout 6eb4205a1d876e500c1cbfd0d8ef651e27c242f7 && cd ..
cd dune-geometry && git checkout 8e5075fc4c58e116d4a0f003a4d1f0482a3ab618 && cd ..
cd dune-grid && git checkout 2749fb531fc2ab027ca728f69634478b9ec9b814 && cd ..
cd dune-istl && git checkout e460af691168a6a9a7509aad1b13534e430f637c && cd ..
cd dune-localfunctions && git checkout eedfe2a7639be4a7e6dc457fc5181454088aebe0 && cd ..
cd dune-multidomain && git checkout deac3cecfc6697c1f5316d55c0fadd74f51d92bc && cd ..
cd dune-multidomaingrid && git checkout 528c09d025dabf460c069b0fcbdc1810adb08ee2 && cd ..
cd dune-pdelab && git checkout 1096e4bd80e13e2d56f6014b2e5120eca419bb8a && cd ..
cd dumux && git checkout faf24fb07f2ba6187a7a66c0bd99aaaffda7b373 && cd ..
# cd dumux-Fritsch2014a && git checkout 5dd2f0d243ddb374a88a0ac650ecaa669affe81b && cd ..

### Patching th modules
cd dune-pdelab && patch -p1 < ../dumux-Fritsch2014a/dune-pdelab.patch && cd ..
cd dumux && patch -p0 < ../dumux-Fritsch2014a/dumux-stable.patch && cd ..

### EXTERNAL MODULES: UG is required
mkdir external
cd external
git clone https://gitlab.dune-project.org/ug/ug.git
cd ug && git checkout -q v3.12.1 && cd ..
cd ug
#wget http://conan.iwr.uni-heidelberg.de/download/ug-3.12.1.tar.gz
#tar xvf ug-3.12.1.tar.gz
#cd ug-3.12.1
sed -i 's/NDELEM_BLKS_MAX                 100/NDELEM_BLKS_MAX                 2000/g' gm/gm.h
autoreconf -is
OPTIM_FLAGS="-O3 -DNDEBUG -march=native -finline-functions -funroll-loops"
CFLAGS="$OPTIM_FLAGS"
CXXFLAGS="$OPTIM_FLAGS -std=c++0x -fno-strict-aliasing"
OPTS="--enable-dune --prefix=$PWD --without-mpi"
./configure CC=g++ CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" $OPTS
make -j3
make install
#./dumux-Fritsch2014a/installexternal.sh ug

### Run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" dumux-Fritsch2014a/automake.opts > dumux-Fritsch2014a/automake_used.opts
./dune-common/bin/dunecontrol --opts=dumux-Fritsch2014a/automake_used.opts all
